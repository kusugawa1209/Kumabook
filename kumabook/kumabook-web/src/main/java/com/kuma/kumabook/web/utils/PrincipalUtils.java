package com.kuma.kumabook.web.utils;

import java.security.Principal;
import java.util.Map;

import com.kuma.kumabook.base.GsonConverter;
import com.kuma.kumabook.base.utils.MapUtils2;
import com.kuma.kumabook.core.vo.PrincipalUserInfo;

public class PrincipalUtils {
	@SuppressWarnings("unchecked")
	public static PrincipalUserInfo getUserInforFromPrincipal(Principal principal) {
		Map<String, Object> principalMap = GsonConverter.convert(principal, Map.class);
		Map<String, Object> userInfoMap = MapUtils2.getValue(principalMap, "userAuthentication.details");

		return GsonConverter.convert(userInfoMap, PrincipalUserInfo.class);
	}
	
	public static long getUserIdFromPrincipal(Principal principal) {
		PrincipalUserInfo principalUserInfo = getUserInforFromPrincipal(principal);
		return principalUserInfo.getId();
	}
}
