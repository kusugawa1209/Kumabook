package com.kuma.kumabook.web.form;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.kuma.kumabook.base.enums.PageNameEnum;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class AddingCreditCardForm extends CreditCardForm {

	@NotNull
	@NotBlank
	private String pageName;

	@AssertTrue
	public boolean isValidPage() {
		PageNameEnum[] pages = PageNameEnum.values();
		for (PageNameEnum page : pages) {
			if (page.getPageName().equals(this.pageName)) {
				return true;
			}
		}

		return false;
	}
}
