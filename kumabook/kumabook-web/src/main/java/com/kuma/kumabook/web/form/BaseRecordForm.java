package com.kuma.kumabook.web.form;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import com.kuma.kumabook.base.enums.RecordTypeEnum;
import com.kuma.kumabook.base.enums.ToolTypeEnum;

import lombok.Data;

@Data
public class BaseRecordForm {

	protected Long id;

	@NotNull
	@NotBlank
	protected String recordType;

	@NotNull
	@NotBlank
	protected String toolType;

	@NotNull
	@NotBlank
	protected String name;

	@Min(0)
	@NotNull
	protected int money;

	@AssertTrue
	public boolean isValidRecordType() {
		RecordTypeEnum[] recordTypes = RecordTypeEnum.values();
		for (RecordTypeEnum type : recordTypes) {
			if (type.name().equals(this.recordType)) {
				return true;
			}
		}

		return false;
	}
	
	@AssertTrue
	public boolean isValidToolType() {
		ToolTypeEnum[] toolTypes = ToolTypeEnum.values();
		for (ToolTypeEnum type : toolTypes) {
			if (type.name().equals(this.toolType)) {
				return true;
			}
		}
		
		return false;
	}
}
