package com.kuma.kumabook.web.view;

import java.io.Serializable;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor(access = AccessLevel.PUBLIC)
@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class ToolTypeView implements Serializable, Comparable<ToolTypeView> {

	private static final long serialVersionUID = 1L;

	private String name;

	private String displayName;

	@Override
	public int compareTo(ToolTypeView o) {
		return name.compareTo(o.name);
	}
}
