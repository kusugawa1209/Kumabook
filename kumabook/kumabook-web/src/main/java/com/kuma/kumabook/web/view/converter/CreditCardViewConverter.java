package com.kuma.kumabook.web.view.converter;

import org.springframework.stereotype.Component;

import com.kuma.kumabook.base.GenericConverter;
import com.kuma.kumabook.core.model.CreditCard;
import com.kuma.kumabook.web.view.CreditCardView;

@Component
public class CreditCardViewConverter implements GenericConverter<CreditCardView> {

	public CreditCardView convert(CreditCard source) {

		if (source == null) {
			return null;
		}

		CreditCardView target = new CreditCardView();
		target.setId(source.getId());
		target.setName(source.getName());
		target.setBillingDay(source.getBillingDay());
		
		return target;
	}

}
