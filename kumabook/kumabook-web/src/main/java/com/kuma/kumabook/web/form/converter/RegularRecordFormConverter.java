package com.kuma.kumabook.web.form.converter;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.kumabook.base.GenericConverter;
import com.kuma.kumabook.core.model.RegularRecord;
import com.kuma.kumabook.core.service.RegularRecordService;
import com.kuma.kumabook.web.form.AddingRegularRecordForm;
import com.kuma.kumabook.web.form.RegularRecordForm;

@Component
public class RegularRecordFormConverter implements GenericConverter<RegularRecord> {

	@Autowired
	private RegularRecordService regularRecordService;

	public RegularRecord convert(RegularRecordForm source) {

		if (source == null) {
			return null;
		}

		Date now = new Date();

		RegularRecord target = new RegularRecord();

		Long regularRecordId = source.getId();
		if (regularRecordId != null) {
			target = regularRecordService.getRegularRecordById(regularRecordId);

		} else {
			target.setCreationDate(now);
		}

		target.setName(source.getName());
		target.setMoney(source.getMoney());
		target.setRecordType(source.getRecordType());
		target.setToolType(source.getToolType());
		target.setNote(source.getNote());
		target.setModificationDate(now);

		return target;
	}

	public RegularRecord convert(AddingRegularRecordForm source) {

		if (source == null) {
			return null;
		}

		Date now = new Date();

		RegularRecord target = new RegularRecord();

		Long regularRecordId = source.getId();
		if (regularRecordId != null) {
			target = regularRecordService.getRegularRecordById(regularRecordId);

		} else {
			target.setCreationDate(now);
		}

		target.setName(source.getName());
		target.setMoney(source.getMoney());
		target.setRecordType(source.getRecordType());
		target.setToolType(source.getToolType());
		target.setNote(source.getNote());
		target.setModificationDate(now);

		return target;
	}
}
