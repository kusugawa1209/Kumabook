package com.kuma.kumabook.web.view.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.kumabook.base.GenericConverter;
import com.kuma.kumabook.base.enums.RecordTypeEnum;
import com.kuma.kumabook.base.enums.ToolTypeEnum;
import com.kuma.kumabook.core.model.CreditCard;
import com.kuma.kumabook.core.model.RegularRecord;
import com.kuma.kumabook.core.service.CreditCardService;
import com.kuma.kumabook.web.view.RegularRecordView;

@Component
public class RegularRecordViewConverter implements GenericConverter<RegularRecordView> {

	@Autowired
	private CreditCardService creditCardService;
	
	public RegularRecordView convert(RegularRecord source) {

		if (source == null) {
			return null;
		}

		RegularRecordView target = new RegularRecordView();

		target.setId(source.getId());
		target.setMoney(source.getMoney());
		target.setName(source.getName());
		
		RecordTypeEnum recordType = RecordTypeEnum.valueOf(source.getRecordType());
		target.setRecordTypeName(recordType.name());
		target.setRecordTypeDisplayName(recordType.getDisplayName());
		
		String toolTypeName = source.getToolType();
		target.setToolTypeName(toolTypeName);

		try {
			ToolTypeEnum toolType = ToolTypeEnum.valueOf(toolTypeName);
			target.setToolTypeDisplayName(toolType.getDisplayName());
		} catch (Exception e) {
			CreditCard creditCard = creditCardService.getCreditCardById(Long.valueOf(toolTypeName));
			target.setToolTypeDisplayName(creditCard.getName());
		}
		
		
		target.setNote(source.getNote());
		target.setCreationDate(source.getCreationDate());
		target.setModificationDate(source.getModificationDate());
		return target;
	}

}
