package com.kuma.kumabook.web.form;

import java.util.Date;

import javax.validation.constraints.NotNull;

import groovy.transform.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
public class RecordForm extends BaseRecordForm {

	@NotNull
	protected Date recordDate;
	
	@NotNull
	protected int recordLayaway;

}
