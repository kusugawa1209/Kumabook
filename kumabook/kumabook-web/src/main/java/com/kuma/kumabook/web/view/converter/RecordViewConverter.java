package com.kuma.kumabook.web.view.converter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.kumabook.base.GenericConverter;
import com.kuma.kumabook.base.enums.RecordTypeEnum;
import com.kuma.kumabook.base.enums.ToolTypeEnum;
import com.kuma.kumabook.core.model.CreditCard;
import com.kuma.kumabook.core.model.Record;
import com.kuma.kumabook.core.service.CreditCardService;
import com.kuma.kumabook.web.view.RecordView;

@Component
public class RecordViewConverter implements GenericConverter<RecordView> {

	@Autowired
	private CreditCardService creditCardService;

	public RecordView convert(Record source) {

		if (source == null) {
			return null;
		}

		RecordView target = new RecordView();

		target.setId(source.getId());
		target.setRecordDate(source.getDate());
		target.setName(source.getItemName());
		target.setRecordLayaway(source.getLayaway());
		target.setMoney(source.getMoney());

		String recordTypeName = source.getRecordType();
		target.setRecordTypeName(recordTypeName);

		RecordTypeEnum recordType = RecordTypeEnum.valueOf(recordTypeName);
		target.setRecordTypeDisplayName(recordType.getDisplayName());

		String toolTypeName = source.getToolType();
		target.setToolTypeName(toolTypeName);

		try {
			ToolTypeEnum toolType = ToolTypeEnum.valueOf(toolTypeName);
			target.setToolTypeDisplayName(toolType.getDisplayName());
		} catch (Exception e) {
			CreditCard creditCard = creditCardService.getCreditCardById(Long.valueOf(toolTypeName));
			target.setToolTypeDisplayName(creditCard.getName());
		}

		return target;
	}

}
