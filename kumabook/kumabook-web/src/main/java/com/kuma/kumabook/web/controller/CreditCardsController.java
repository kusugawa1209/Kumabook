package com.kuma.kumabook.web.controller;

import static com.kuma.kumabook.web.utils.PageSettingUtils.appendPages;
import static com.kuma.kumabook.web.utils.PageSettingUtils.getPageSize;
import static com.kuma.kumabook.web.utils.PrincipalUtils.getUserIdFromPrincipal;

import java.security.Principal;
import java.text.ParseException;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kuma.kumabook.base.queryondition.PageCondition;
import com.kuma.kumabook.core.model.CreditCard;
import com.kuma.kumabook.core.model.User;
import com.kuma.kumabook.core.service.CreditCardService;
import com.kuma.kumabook.core.service.UserService;
import com.kuma.kumabook.web.form.AddingCreditCardForm;
import com.kuma.kumabook.web.form.CreditCardForm;
import com.kuma.kumabook.web.form.converter.CreditCardFormConverter;
import com.kuma.kumabook.web.view.CreditCardView;
import com.kuma.kumabook.web.view.CreditCardsView;
import com.kuma.kumabook.web.view.converter.CreditCardViewConverter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/creditCards")
public class CreditCardsController {

	@Autowired
	private UserService userService;

	@Autowired
	private CreditCardService creditCardService;

	@Autowired
	private CreditCardFormConverter creditCardFormConverter;

	@Autowired
	private CreditCardViewConverter creditCardViewConverter;

	@RequestMapping("/{pageNumber}")
	public String showCreditCardsByPageNumber(Principal principal, Device device, Model model,
			@PathVariable("pageNumber") int pageNumber) {

		appendCreditCards(principal, device, model, pageNumber);

		return "creditCards";
	}

	@RequestMapping("/adding")
	public String showAddingCreditCardView(Model model) {

		CreditCardView defaultCreditCard = createDefaultCreditCard();
		model.addAttribute("creditCard", defaultCreditCard);

		return "addingCreditCard";
	}

	@RequestMapping("/editing/{creditCardId}")
	public String showEditingCreditCardView(Principal principal, Model model,
			@PathVariable("creditCardId") long creditCardId) {

		CreditCard creditCard = creditCardService.getCreditCardById(creditCardId);
		if (creditCard == null) {
			return "redicect:/creditCards/adding";
		}

		long principalUserId = getUserIdFromPrincipal(principal);
		long recordUserId = creditCard.getUser().getId();
		if (Long.compare(principalUserId, recordUserId) != 0) {
			return "redicect:/creditCards/adding";
		}

		CreditCardView creditCardView = creditCardViewConverter.convert(creditCard);
		model.addAttribute("creditCard", creditCardView);

		return "editingCreditCard";
	}

	@PostMapping("/add")
	public String addCreditCard(Principal principal, @ModelAttribute AddingCreditCardForm addingCreditCardForm)
			throws ParseException {

		long userId = getUserIdFromPrincipal(principal);
		User user = userService.getUserById(userId);

		CreditCard creditCard = creditCardFormConverter.convert(addingCreditCardForm);
		creditCard = creditCardService.saveCreditCard(user, creditCard);
		log.info("Save credit card {}", creditCard);

		return "redirect:/" + addingCreditCardForm.getPageName();
	}

	@PostMapping("/save")
	public String saveCreditCard(Principal principal, @ModelAttribute CreditCardForm creditCardForm) {

		CreditCard creditCard = creditCardFormConverter.convert(creditCardForm);
		long recordUserId = creditCard.getUser().getId();
		long principalUserId = getUserIdFromPrincipal(principal);

		if (Long.compare(recordUserId, principalUserId) != 0) {
			return "redirect:/creditCards/1";
		}

		creditCard = creditCardService.saveCreditCard(creditCard);
		log.info("Save credit card {}", creditCard);

		return "redirect:/creditCards/1";
	}

	@RequestMapping("/remove/{creditCardId}")
	public String removeCreditCard(Principal principal, Model model, @PathVariable("creditCardId") long creditCardId) {
		long principalUserId = getUserIdFromPrincipal(principal);

		CreditCard creditCard = creditCardService.getCreditCardById(creditCardId);
		if (creditCard == null) {
			return "redirect:/creditCards/1";
		}

		long recordUserId = creditCard.getUser().getId();
		if (Long.compare(principalUserId, recordUserId) != 0) {
			return "redirect:/creditCards/1";
		}

		creditCardService.removeCreditCard(creditCard);
		return "redirect:/creditCards/1";
	}

	private void appendCreditCards(Principal principal, Device device, Model model, int pageNumber) {
		long userId = getUserIdFromPrincipal(principal);

		int pageSize = getPageSize(device);
		int total = creditCardService.getCreditCardsCountByUserId(userId);

		PageCondition pageCondition = new PageCondition();
		pageCondition.setPageNumber(pageNumber - 1);
		pageCondition.setPageSize(pageSize);

		List<CreditCard> creditCards = creditCardService.findCreditCardsByUserId(userId, pageCondition);
		List<CreditCardView> creditCardViews = creditCardViewConverter.convert(creditCards);
		Collections.sort(creditCardViews);

		CreditCardsView creditCardsView = new CreditCardsView();

		creditCardsView.setCreditCards(creditCardViews);
		creditCardsView.setTotal(total);
		creditCardsView.setCurrentPage(pageNumber);
		creditCardsView.setPageSize(pageSize);

		appendPages(creditCardsView, device, total, pageSize, pageNumber);

		model.addAttribute("creditCards", creditCardsView);
	}

	private CreditCardView createDefaultCreditCard() {
		CreditCardView defaultCreditCard = new CreditCardView();

		defaultCreditCard.setName(StringUtils.EMPTY);
		defaultCreditCard.setBillingDay(1);

		return defaultCreditCard;
	}
}
