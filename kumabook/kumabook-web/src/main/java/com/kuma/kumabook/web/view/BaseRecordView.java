package com.kuma.kumabook.web.view;

import java.io.Serializable;

import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(of = "id")
public class BaseRecordView implements Serializable {

	private static final long serialVersionUID = 1L;

	private long id;

	private String name;

	private int money;

	private String recordTypeName;

	private String recordTypeDisplayName;

	private String toolTypeName;

	private String ToolTypeDisplayName;

}
