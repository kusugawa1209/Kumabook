package com.kuma.kumabook.web.view;

import java.io.Serializable;
import java.util.List;

import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RegularRecordsView extends PagingDataView implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<RegularRecordView> regularRecords = Lists.newArrayList();

}
