package com.kuma.kumabook.web.view.converter;

import org.springframework.stereotype.Component;

import com.kuma.kumabook.base.GenericConverter;
import com.kuma.kumabook.core.model.User;
import com.kuma.kumabook.web.view.UserView;

@Component
public class UserViewConverter implements GenericConverter<UserView> {

	public UserView convert(User source) {

		if (source == null) {
			return null;
		}

		UserView target = new UserView();
		target.setUsername(source.getUsername());
		
		return target;
	}
}
