package com.kuma.kumabook.web.view;

import java.io.Serializable;

import lombok.Data;

@Data
public class PagingDataView implements Serializable {

	private static final long serialVersionUID = 1L;

	private int total;

	private int currentPage;

	private int minPage;

	private int maxPage;

	private int pageSize;
	
	private int pages;
}
