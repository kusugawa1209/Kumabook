package com.kuma.kumabook.web.utils;

import org.springframework.mobile.device.Device;

import com.kuma.kumabook.web.view.PagingDataView;

public class PageSettingUtils {

	private static final int MOBILE_PAGE_SIZE = 15;

	private static final int NORMAL_PAGE_SIZE = 30;

	private static final int MOBILE_PAGE_NUMBERS_SIZE = 3;

	private static final int NORMAL_PAGE_NUMBERS_SIZE = 11;

	public static int getPageSize(Device device) {
		if (device.isMobile()) {
			return MOBILE_PAGE_SIZE;
		} else {
			return NORMAL_PAGE_SIZE;
		}
	}

	public static int getPageNumbersSize(Device device) {
		if (device.isMobile()) {
			return MOBILE_PAGE_NUMBERS_SIZE;
		} else {
			return NORMAL_PAGE_NUMBERS_SIZE;
		}
	}

	public static <DATAVIEW extends PagingDataView> void appendPages(DATAVIEW dataView, Device device, int total,
			int pageSize, int currentPage) {
		int pages = total / pageSize;

		if (total % pageSize != 0) {
			pages += 1;
		}
		dataView.setPages(pages);

		int pageNumberSize = getPageNumbersSize(device);

		if (pages <= pageNumberSize) {
			dataView.setMinPage(1);
			dataView.setMaxPage(pages);
		} else {

			int halfPageNumberSize = pageNumberSize / 2;
			int minPage = currentPage - halfPageNumberSize;
			int maxPage = currentPage + halfPageNumberSize;

			while (minPage < 1) {
				minPage++;
				maxPage++;
			}

			while (maxPage > pages) {
				minPage--;
				maxPage--;
			}

			dataView.setMinPage(minPage);
			dataView.setMaxPage(maxPage);
		}
	}
}
