package com.kuma.kumabook.web.form;

import javax.validation.constraints.NotNull;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RegularRecordForm extends BaseRecordForm {

	@NotNull
	protected String note;
	
}
