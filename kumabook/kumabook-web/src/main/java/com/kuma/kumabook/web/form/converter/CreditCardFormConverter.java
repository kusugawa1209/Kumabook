package com.kuma.kumabook.web.form.converter;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.kumabook.base.GenericConverter;
import com.kuma.kumabook.core.model.CreditCard;
import com.kuma.kumabook.core.service.CreditCardService;
import com.kuma.kumabook.web.form.AddingCreditCardForm;
import com.kuma.kumabook.web.form.CreditCardForm;

@Component
public class CreditCardFormConverter implements GenericConverter<CreditCard> {

	@Autowired
	private CreditCardService creditCardService;

	public CreditCard convert(CreditCardForm source) {

		if (source == null) {
			return null;
		}

		Date now = new Date();

		CreditCard target = new CreditCard();

		Long creditCardId = source.getId();
		if (creditCardId != null) {
			target = creditCardService.getCreditCardById(creditCardId);
		} else {
			target.setCreationDate(now);
		}

		target.setId(creditCardId);
		target.setName(source.getName());
		target.setBillingDay(source.getBillingDay());
		target.setModificationDate(now);

		return target;
	}

	public CreditCard convert(AddingCreditCardForm source) {

		if (source == null) {
			return null;
		}

		Date now = new Date();

		CreditCard target = new CreditCard();

		Long creditCardId = source.getId();
		if (creditCardId != null) {
			target = creditCardService.getCreditCardById(creditCardId);
		} else {
			target.setCreationDate(now);
		}

		target.setId(creditCardId);
		target.setName(source.getName());
		target.setBillingDay(source.getBillingDay());
		target.setModificationDate(now);

		return target;
	}
}
