package com.kuma.kumabook.web.view;

import java.io.Serializable;

import lombok.Data;

@Data
public class UserView implements Serializable, Comparable<UserView> {

	private static final long serialVersionUID = 1L;

	private String username;

	@Override
	public int compareTo(UserView o) {
		return this.username.compareTo(o.username);
	}
}
