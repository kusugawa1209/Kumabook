package com.kuma.kumabook.web.form;

import lombok.Data;

@Data
public class SearchingRecordForm {

	private String fromDateValue;

	private String toDateValue;

	private String recordType;

	private String toolType;

	private String name;

	private Integer fromMoney;

	private Integer toMoney;
}
