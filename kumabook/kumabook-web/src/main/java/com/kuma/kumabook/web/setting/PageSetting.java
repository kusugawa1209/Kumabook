package com.kuma.kumabook.web.setting;

public interface PageSetting {
	
	public static final int MOBILE_PAGE_SIZE = 8;

	public static final int NORMAL_PAGE_SIZE = 15;

	public static final int MOBILE_PAGE_NUMBERS_SIZE = 5;

	public static final int NORMAL_PAGE_NUMBERS_SIZE = 10;
	
}
