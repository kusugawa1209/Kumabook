package com.kuma.kumabook.web.view.converter;

import org.springframework.stereotype.Component;

import com.kuma.kumabook.base.GenericConverter;
import com.kuma.kumabook.core.vo.RecordType;
import com.kuma.kumabook.web.view.RecordTypeView;

@Component
public class RecordTypeViewConverter implements GenericConverter<RecordTypeView> {

	public RecordTypeView convert(RecordType source) {

		if (source == null) {
			return null;
		}

		RecordTypeView target = new RecordTypeView();
		target.setName(source.getName());
		target.setDisplayName(source.getDisplayName());

		return target;
	}

}
