package com.kuma.kumabook.web.controller.rest;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@RestController
public class WakeUpController {

	@RequestMapping("/wake")
	public String wakeup() throws Exception {
		String wakeup = "OK, I wake up.";
		log.debug(wakeup);

		return wakeup;
	}
}
