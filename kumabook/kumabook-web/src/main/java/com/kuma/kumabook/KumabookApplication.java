package com.kuma.kumabook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@SpringBootApplication(scanBasePackages = { "com.kuma.kumabook" })
@EnableScheduling
@EnableAsync
@EnableOAuth2Sso
@EnableJpaRepositories
@EntityScan
public class KumabookApplication extends WebSecurityConfigurerAdapter {

	public static void main(String[] args) {
		SpringApplication.run(KumabookApplication.class, args);
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		 http.authorizeRequests().antMatchers("/login**", "/webjars/**", "/.well-known/**").permitAll()
         .and()
         	.antMatcher("/**").authorizeRequests().anyRequest().authenticated()
         .and()
         	.sessionManagement().invalidSessionUrl("/");
	}
}
