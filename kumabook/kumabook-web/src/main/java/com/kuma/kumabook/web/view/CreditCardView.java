package com.kuma.kumabook.web.view;

import java.io.Serializable;

import lombok.Data;

@Data
public class CreditCardView implements Serializable, Comparable<CreditCardView> {

	private static final long serialVersionUID = 1L;

	private long id;

	private String name;

	private int billingDay;

	@Override
	public int compareTo(CreditCardView o) {
		return this.name.compareTo(o.name);
	}

}
