package com.kuma.kumabook.web.controller.rest;

import static org.springframework.http.MediaType.APPLICATION_FORM_URLENCODED_VALUE;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import java.security.Principal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kuma.kumabook.core.model.Record;
import com.kuma.kumabook.core.service.RecordService;
import com.kuma.kumabook.core.specification.RecordSearchCondition;
import com.kuma.kumabook.web.form.SearchingRecordForm;
import com.kuma.kumabook.web.utils.PrincipalUtils;
import com.kuma.kumabook.web.view.RecordView;
import com.kuma.kumabook.web.view.converter.RecordViewConverter;

@RestController
@RequestMapping("/searching")
public class RecordsSearchController {

	@Autowired
	private RecordService recordService;

	@Autowired
	private RecordViewConverter recordViewConverter;

	@PostMapping(path = "/record", consumes = APPLICATION_FORM_URLENCODED_VALUE, produces = APPLICATION_JSON_VALUE)
	public List<RecordView> searchRecord(Principal principal, SearchingRecordForm searchingRecordForm)
			throws ParseException {

		DateFormat df = new SimpleDateFormat("yyyy/MM/dd");

		RecordSearchCondition recordSpecification = new RecordSearchCondition();

		recordSpecification.setUserId(PrincipalUtils.getUserIdFromPrincipal(principal));

		String fromDateValue = searchingRecordForm.getFromDateValue();
		if (StringUtils.isNotEmpty(fromDateValue)) {
			Date fromDate = df.parse(fromDateValue);
			recordSpecification.setFromDate(fromDate);
		} else {
			recordSpecification.setFromDate(null);
		}

		String toDateValue = searchingRecordForm.getToDateValue();
		if (StringUtils.isNotEmpty(toDateValue)) {
			Date toDate = df.parse(toDateValue);
			recordSpecification.setToDate(toDate);
		} else {
			recordSpecification.setToDate(null);
		}

		recordSpecification.setFromMoney(searchingRecordForm.getFromMoney());
		recordSpecification.setName(searchingRecordForm.getName());
		recordSpecification.setRecordType(searchingRecordForm.getRecordType());
		recordSpecification.setToMoney(searchingRecordForm.getToMoney());
		recordSpecification.setToolType(searchingRecordForm.getToolType());

		List<Record> records = recordService.findRecords(recordSpecification);

		return recordViewConverter.convert(records);
	}
}
