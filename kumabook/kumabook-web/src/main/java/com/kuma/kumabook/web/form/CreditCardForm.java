package com.kuma.kumabook.web.form;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Data;

@Data
public class CreditCardForm {

	private Long id;

	@NotNull
	@NotBlank
	private String name;

	@NotNull
	private int billingDay;
	
}
