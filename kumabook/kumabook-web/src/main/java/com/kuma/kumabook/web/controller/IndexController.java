package com.kuma.kumabook.web.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class IndexController {

	@Value("${customized.application.version}")
	private String applicationVersion;

	@Value("${customized.application.name}")
	private String applicationName;

	@RequestMapping("/")
	public String loginIndex() {
		return "index";
	}
}
