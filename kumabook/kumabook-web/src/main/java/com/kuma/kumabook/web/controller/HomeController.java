package com.kuma.kumabook.web.controller;

import static com.kuma.kumabook.web.utils.PrincipalUtils.getUserIdFromPrincipal;

import java.security.Principal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kuma.kumabook.core.service.AssetsService;
import com.kuma.kumabook.core.service.BudgetService;
import com.kuma.kumabook.core.vo.Assets;
import com.kuma.kumabook.core.vo.Budget;

@Controller
public class HomeController {

	@Autowired
	private BudgetService budgetService;

	@Autowired
	private AssetsService assetsService;

	@RequestMapping("/home")
	public String home(Principal principal, Device device, Model model) {
		long userId = getUserIdFromPrincipal(principal);

		List<Budget> budgets = budgetService.findAllBudgets(userId, 2);
		model.addAttribute("budgets", budgets);

		Budget todayBudget = budgetService.getTodayBudgetByUserId(userId);
		model.addAttribute("todayBudget", todayBudget);

		Budget monthBudget = budgetService.getMonthlyBudgetByUserId(userId);
		model.addAttribute("monthBudget", monthBudget);

		Assets assets = assetsService.getAssetsByUserId(userId);
		model.addAttribute("assets", assets);

		return "home";
	}
}
