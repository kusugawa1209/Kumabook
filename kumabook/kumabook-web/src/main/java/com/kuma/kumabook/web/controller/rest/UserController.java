package com.kuma.kumabook.web.controller.rest;

import static com.kuma.kumabook.web.utils.PrincipalUtils.getUserInforFromPrincipal;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kuma.kumabook.core.model.User;
import com.kuma.kumabook.core.service.UserService;
import com.kuma.kumabook.core.vo.PrincipalUserInfo;
import com.kuma.kumabook.web.view.UserView;
import com.kuma.kumabook.web.view.converter.UserViewConverter;

@RestController
public class UserController {

	@Autowired
	private UserService userService;

	@Autowired
	private UserViewConverter userViewConverter;

	@RequestMapping("/user")
	public UserView user(Principal principal) {
		PrincipalUserInfo principalUserInfo = getUserInforFromPrincipal(principal);
		long userId = principalUserInfo.getId();
		User user = userService.getUserById(userId);

		if (user == null) {
			String username = principalUserInfo.getName();
			user = userService.createAndSaveUser(userId, username);
		}

		return userViewConverter.convert(user);
	}
}
