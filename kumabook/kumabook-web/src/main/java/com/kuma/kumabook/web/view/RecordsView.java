package com.kuma.kumabook.web.view;

import java.io.Serializable;
import java.util.List;

import com.google.common.collect.Lists;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RecordsView extends PagingDataView implements Serializable {

	private static final long serialVersionUID = 1L;

	private List<RecordView> records = Lists.newArrayList();
}
