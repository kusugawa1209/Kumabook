package com.kuma.kumabook.web.form.converter;

import java.text.ParseException;
import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.kuma.kumabook.base.GenericConverter;
import com.kuma.kumabook.core.model.Record;
import com.kuma.kumabook.core.service.RecordService;
import com.kuma.kumabook.web.form.AddingRecordForm;
import com.kuma.kumabook.web.form.RecordForm;

@Component
public class RecordFormConverter implements GenericConverter<Record> {

	@Autowired
	private RecordService recordService;

	public Record convert(RecordForm source) throws ParseException {

		if (source == null) {
			return null;
		}

		Date now = new Date();

		Record target = new Record();

		Long recordId = source.getId();
		if (recordId != null) {
			target = recordService.getRecordById(recordId);

		} else {
			target.setCreationDate(now);
		}

		target.setDate(source.getRecordDate());
		target.setItemName(source.getName());
		target.setMoney(source.getMoney());
		target.setLayaway(source.getRecordLayaway());
		target.setRecordType(source.getRecordType());
		target.setToolType(source.getToolType());
		target.setNote(StringUtils.EMPTY);
		target.setModificationDate(now);
		target.setIsAutoAdded(false);

		return target;
	}

	public Record convert(AddingRecordForm source) throws ParseException {

		if (source == null) {
			return null;
		}

		Date now = new Date();

		Record target = new Record();

		Long recordId = source.getId();
		if (recordId != null) {
			target = recordService.getRecordById(recordId);

		} else {
			target.setCreationDate(now);
		}

		target.setDate(source.getRecordDate());
		target.setItemName(source.getName());
		target.setMoney(source.getMoney());
		target.setLayaway(source.getRecordLayaway());
		target.setRecordType(source.getRecordType());
		target.setToolType(source.getToolType());
		target.setNote(StringUtils.EMPTY);
		target.setModificationDate(now);
		target.setIsAutoAdded(false);

		return target;
	}
}
