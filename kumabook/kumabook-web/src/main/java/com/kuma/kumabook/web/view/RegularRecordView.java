package com.kuma.kumabook.web.view;

import java.io.Serializable;
import java.util.Date;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
public class RegularRecordView extends BaseRecordView implements Serializable, Comparable<RegularRecordView> {

	private static final long serialVersionUID = 1L;

	private String note;

	private Date creationDate;

	private Date modificationDate;

	@Override
	public int compareTo(RegularRecordView o) {
		if (!modificationDate.equals(o.modificationDate)) {
			// date DESC
			return o.modificationDate.compareTo(modificationDate);
		} else {
			return this.getToolTypeName().compareTo(o.getToolTypeName());
		}
	}
}
