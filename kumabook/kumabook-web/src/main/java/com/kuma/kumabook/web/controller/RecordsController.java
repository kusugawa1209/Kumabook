package com.kuma.kumabook.web.controller;

import static com.kuma.kumabook.base.enums.RecordTypeEnum.OUTGOING;
import static com.kuma.kumabook.base.enums.ToolTypeEnum.CASH;
import static com.kuma.kumabook.web.utils.PrincipalUtils.getUserIdFromPrincipal;

import java.security.Principal;
import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.google.common.collect.Maps;
import com.kuma.kumabook.base.queryondition.PageCondition;
import com.kuma.kumabook.core.model.Record;
import com.kuma.kumabook.core.model.User;
import com.kuma.kumabook.core.service.RecordService;
import com.kuma.kumabook.core.service.RecordTypeService;
import com.kuma.kumabook.core.service.ToolTypeService;
import com.kuma.kumabook.core.service.UserService;
import com.kuma.kumabook.core.vo.RecordType;
import com.kuma.kumabook.core.vo.ToolType;
import com.kuma.kumabook.web.form.AddingRecordForm;
import com.kuma.kumabook.web.form.RecordForm;
import com.kuma.kumabook.web.form.converter.RecordFormConverter;
import com.kuma.kumabook.web.utils.PageSettingUtils;
import com.kuma.kumabook.web.utils.PrincipalUtils;
import com.kuma.kumabook.web.view.RecordTypeView;
import com.kuma.kumabook.web.view.RecordView;
import com.kuma.kumabook.web.view.RecordsView;
import com.kuma.kumabook.web.view.ToolTypeView;
import com.kuma.kumabook.web.view.converter.RecordTypeViewConverter;
import com.kuma.kumabook.web.view.converter.RecordViewConverter;
import com.kuma.kumabook.web.view.converter.ToolTypeViewConverter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/records")
public class RecordsController {

	@Autowired
	private UserService userService;

	@Autowired
	private RecordService recordService;

	@Autowired
	private ToolTypeService toolTypeService;

	@Autowired
	private RecordTypeService recordTypeService;

	@Autowired
	private RecordTypeViewConverter recordTypeViewConverter;

	@Autowired
	private ToolTypeViewConverter toolTypeViewConverter;

	@Autowired
	private RecordFormConverter recordFormConverter;

	@Autowired
	private RecordViewConverter recordViewConverter;

	private static final String RECORD = "record";

	private static final String TOOL_TYPES = "toolTypes";

	private static final String RECORD_TYPES = "recordTypes";
	
	private static final String REDIRECT_TO_RECORDS_PAGE = "redirect:/records/1";

	@RequestMapping("/{pageNumber}")
	public String showRecordsByPageNumber(Principal principal, Device device, Model model,
			@PathVariable("pageNumber") int pageNumber) {

		appendRecords(principal, device, model, pageNumber);
		appendRecordTypes(model);

		return "records";
	}

	@RequestMapping("/adding")
	public String showAddingRecordView(Principal principal, Model model) {

		appendRecordTypes(model);

		long userId = PrincipalUtils.getUserIdFromPrincipal(principal);
		appendToolTypes(userId, model);

		RecordView defaultRecord = createDefaultRecord();

		model.addAttribute(RECORD, defaultRecord);

		return "addingRecord";
	}

	@RequestMapping("/searching")
	public String showSearchingRecordView(Principal principal, Model model) {
		List<RecordTypeView> recordTypeViews = getRecordTypeViews();
		recordTypeViews.add(0, new RecordTypeView("NONE", "請選擇"));
		model.addAttribute(RECORD_TYPES, recordTypeViews);

		long userId = PrincipalUtils.getUserIdFromPrincipal(principal);
		List<ToolTypeView> toolTypeViews = getToolTypeViews(userId);
		toolTypeViews.add(0, new ToolTypeView("NONE", "請選擇"));
		model.addAttribute(TOOL_TYPES, toolTypeViews);

		Map<String, Object> record = createDefaultSearchCondition();
		model.addAttribute(RECORD, record);

		return "searchingRecord";
	}

	@RequestMapping("/editing/{recordId}")
	public String showEditingRecordView(Principal principal, Model model, @PathVariable("recordId") long recordId) {

		Record record = recordService.getRecordById(recordId);
		if (record == null) {
			return "redirect:/records/adding";
		}

		long principalUserId = PrincipalUtils.getUserIdFromPrincipal(principal);
		long recordUserId = record.getUser().getId();
		if (Long.compare(principalUserId, recordUserId) != 0) {
			return "redirect:/records/adding";
		}

		RecordView recordView = recordViewConverter.convert(record);
		model.addAttribute(RECORD, recordView);

		appendRecordTypes(model);

		long userId = PrincipalUtils.getUserIdFromPrincipal(principal);
		List<ToolType> toolTypes = toolTypeService.findToolTypesByUserId(userId);
		List<ToolTypeView> toolTypeViews = toolTypeViewConverter.convert(toolTypes);
		model.addAttribute(TOOL_TYPES, toolTypeViews);

		return "editingRecord";
	}

	@PostMapping("/add")
	public String addRecord(Principal principal, @ModelAttribute AddingRecordForm addingRecordForm)
			throws ParseException {

		long userId = PrincipalUtils.getUserIdFromPrincipal(principal);
		User user = userService.getUserById(userId);

		Record record = recordFormConverter.convert(addingRecordForm);
		record = recordService.saveRecord(user, record);
		log.info("Save record {}", record);

		return "redirect:/" + addingRecordForm.getPageName();
	}

	@PostMapping("/save")
	public String saveRecord(Principal principal, @ModelAttribute RecordForm recordForm) throws ParseException {

		Record record = recordFormConverter.convert(recordForm);
		long recordUserId = record.getUser().getId();
		long principalUserId = PrincipalUtils.getUserIdFromPrincipal(principal);

		if (Long.compare(recordUserId, principalUserId) != 0) {
			return REDIRECT_TO_RECORDS_PAGE;
		}

		record = recordService.saveRecord(record);
		log.info("Save record {}", record);

		return REDIRECT_TO_RECORDS_PAGE;
	}

	@RequestMapping("/remove/{recordId}")
	public String removeRecord(Principal principal, Model model, @PathVariable("recordId") long recordId) {
		long principalUserId = PrincipalUtils.getUserIdFromPrincipal(principal);

		Record record = recordService.getRecordById(recordId);
		if (record == null) {
			return REDIRECT_TO_RECORDS_PAGE;
		}

		long recordUserId = record.getUser().getId();
		if (Long.compare(principalUserId, recordUserId) != 0) {
			return REDIRECT_TO_RECORDS_PAGE;
		}

		recordService.removeRecord(record);
		return REDIRECT_TO_RECORDS_PAGE;
	}

	private void appendRecords(Principal principal, Device device, Model model, int pageNumber) {
		long userId = getUserIdFromPrincipal(principal);

		int pageSize = PageSettingUtils.getPageSize(device);
		int total = recordService.getRecordsCountByUserId(userId);

		PageCondition pageCondition = new PageCondition();
		pageCondition.setPageNumber(pageNumber - 1);
		pageCondition.setPageSize(pageSize);

		List<Record> records = recordService.findRecordsByUserId(userId, pageCondition);
		List<RecordView> recordViews = recordViewConverter.convert(records);
		Collections.sort(recordViews);

		RecordsView recordsView = new RecordsView();
		recordsView.setRecords(recordViews);
		recordsView.setTotal(total);
		recordsView.setCurrentPage(pageNumber);
		recordsView.setPageSize(pageSize);

		PageSettingUtils.appendPages(recordsView, device, total, pageSize, pageNumber);

		model.addAttribute("records", recordsView);
	}

	private List<RecordTypeView> getRecordTypeViews() {
		List<RecordType> recordTypes = recordTypeService.findAll();
		return recordTypeViewConverter.convert(recordTypes);
	}

	private void appendRecordTypes(Model model) {
		List<RecordTypeView> recordTypeViews = getRecordTypeViews();
		model.addAttribute(RECORD_TYPES, recordTypeViews);
	}

	private List<ToolTypeView> getToolTypeViews(long userId) {
		List<ToolType> toolTypes = toolTypeService.findToolTypesByUserId(userId);
		return toolTypeViewConverter.convert(toolTypes);
	}

	private void appendToolTypes(long userId, Model model) {
		List<ToolTypeView> toolTypeViews = getToolTypeViews(userId);
		model.addAttribute(TOOL_TYPES, toolTypeViews);
	}

	private RecordView createDefaultRecord() {
		Date today = new Date();

		RecordView defaultRecord = new RecordView();

		defaultRecord.setRecordDate(today);
		defaultRecord.setName(StringUtils.EMPTY);
		defaultRecord.setMoney(0);
		defaultRecord.setRecordLayaway(1);
		defaultRecord.setRecordTypeDisplayName(OUTGOING.getDisplayName());
		defaultRecord.setRecordTypeName(OUTGOING.name());
		defaultRecord.setToolTypeDisplayName(CASH.getDisplayName());
		defaultRecord.setToolTypeName(CASH.name());

		return defaultRecord;
	}

	private Map<String, Object> createDefaultSearchCondition() {
		Map<String, Object> searchCondition = Maps.newHashMap();
		searchCondition.put("recordTypeDisplayName", "請選擇");
		searchCondition.put("recordTypeName", "NONE");
		searchCondition.put("toolTypeDisplayName", "請選擇");
		searchCondition.put("toolTypeName", "NONE");
		searchCondition.put("name", StringUtils.EMPTY);

		return searchCondition;
	}
}
