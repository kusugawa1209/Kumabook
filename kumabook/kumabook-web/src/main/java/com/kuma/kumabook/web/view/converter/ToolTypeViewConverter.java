package com.kuma.kumabook.web.view.converter;

import org.springframework.stereotype.Component;

import com.kuma.kumabook.base.GenericConverter;
import com.kuma.kumabook.core.vo.ToolType;
import com.kuma.kumabook.web.view.ToolTypeView;

@Component
public class ToolTypeViewConverter implements GenericConverter<ToolTypeView> {

	public ToolTypeView convert(ToolType source) {

		if (source == null) {
			return null;
		}

		ToolTypeView target = new ToolTypeView();
		
		target.setName(source.getName());
		target.setDisplayName(source.getDisplayName());

		return target;
	}

}
