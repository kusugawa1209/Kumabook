package com.kuma.kumabook.web.view;

import java.io.Serializable;
import java.util.Date;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = true)
public class RecordView extends BaseRecordView implements Serializable, Comparable<RecordView> {

	private static final long serialVersionUID = 1L;

	private Date recordDate;

	private int recordLayaway;

	@Override
	public int compareTo(RecordView o) {
		if (!recordDate.equals(o.recordDate)) {
			// date DESC
			return o.recordDate.compareTo(recordDate);
		} else {
			return Long.compare(o.getId(), this.getId());
		}
	}
}
