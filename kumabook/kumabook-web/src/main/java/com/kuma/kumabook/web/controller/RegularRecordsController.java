package com.kuma.kumabook.web.controller;

import static com.kuma.kumabook.base.enums.RecordTypeEnum.OUTGOING;
import static com.kuma.kumabook.base.enums.ToolTypeEnum.CASH;
import static com.kuma.kumabook.web.utils.PrincipalUtils.getUserIdFromPrincipal;

import java.security.Principal;
import java.text.ParseException;
import java.util.Collections;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.kuma.kumabook.base.queryondition.PageCondition;
import com.kuma.kumabook.core.model.RegularRecord;
import com.kuma.kumabook.core.model.User;
import com.kuma.kumabook.core.service.RecordTypeService;
import com.kuma.kumabook.core.service.RegularRecordService;
import com.kuma.kumabook.core.service.ToolTypeService;
import com.kuma.kumabook.core.service.UserService;
import com.kuma.kumabook.core.vo.RecordType;
import com.kuma.kumabook.core.vo.ToolType;
import com.kuma.kumabook.web.form.AddingRegularRecordForm;
import com.kuma.kumabook.web.form.RegularRecordForm;
import com.kuma.kumabook.web.form.converter.RegularRecordFormConverter;
import com.kuma.kumabook.web.setting.PageSetting;
import com.kuma.kumabook.web.utils.PageSettingUtils;
import com.kuma.kumabook.web.utils.PrincipalUtils;
import com.kuma.kumabook.web.view.RecordTypeView;
import com.kuma.kumabook.web.view.RegularRecordView;
import com.kuma.kumabook.web.view.RegularRecordsView;
import com.kuma.kumabook.web.view.ToolTypeView;
import com.kuma.kumabook.web.view.converter.RecordTypeViewConverter;
import com.kuma.kumabook.web.view.converter.RegularRecordViewConverter;
import com.kuma.kumabook.web.view.converter.ToolTypeViewConverter;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@RequestMapping("/records/regular")
public class RegularRecordsController implements PageSetting {

	@Autowired
	private UserService userService;

	@Autowired
	private RegularRecordService regularRecordService;

	@Autowired
	private RecordTypeService recordTypeService;

	@Autowired
	private ToolTypeService toolTypeService;

	@Autowired
	private RegularRecordViewConverter regularRecordViewConverter;

	@Autowired
	private RecordTypeViewConverter recordTypeViewConverter;

	@Autowired
	private ToolTypeViewConverter toolTypeViewConverter;

	@Autowired
	private RegularRecordFormConverter regularRecordFormConverter;

	@RequestMapping("/{pageNumber}")
	public String showRegularRecordsByPageNumber(Principal principal, Device device, Model model,
			@PathVariable("pageNumber") int pageNumber) {

		appendRegularRecords(principal, device, model, pageNumber);
		appendRecordTypes(model);

		return "regularRecords";
	}

	@RequestMapping("/adding")
	public String showAddingRegularRecordView(Principal principal, Model model) {

		appendRecordTypes(model);

		long userId = PrincipalUtils.getUserIdFromPrincipal(principal);
		List<ToolType> toolTypes = toolTypeService.findToolTypesByUserId(userId);
		List<ToolTypeView> toolTypeViews = toolTypeViewConverter.convert(toolTypes);
		model.addAttribute("toolTypes", toolTypeViews);

		RegularRecordView defaultRegularRecord = createDefaultRegularRecordView();
		model.addAttribute("regularRecord", defaultRegularRecord);

		return "addingRegularRecord";
	}

	@RequestMapping("/editing/{regularRecordId}")
	public String showEditingRegularRecordView(Principal principal, Model model,
			@PathVariable("regularRecordId") long regularRecordId) {

		RegularRecord regularRecord = regularRecordService.getRegularRecordById(regularRecordId);
		if (regularRecord == null) {
			return "redicect:/records/regular/adding";
		}

		long principalUserId = PrincipalUtils.getUserIdFromPrincipal(principal);
		long recordUserId = regularRecord.getUser().getId();
		if (Long.compare(principalUserId, recordUserId) != 0) {
			return "redicect:/records/regular/adding";
		}

		RegularRecordView regularRecordView = regularRecordViewConverter.convert(regularRecord);
		model.addAttribute("regularRecord", regularRecordView);

		appendRecordTypes(model);

		long userId = PrincipalUtils.getUserIdFromPrincipal(principal);
		List<ToolType> toolTypes = toolTypeService.findToolTypesByUserId(userId);
		List<ToolTypeView> toolTypeViews = toolTypeViewConverter.convert(toolTypes);
		model.addAttribute("toolTypes", toolTypeViews);

		return "editingRegularRecord";
	}

	@PostMapping("/add")
	public String addRegularRecord(Principal principal, @ModelAttribute AddingRegularRecordForm addingRegularRecordForm)
			throws ParseException {

		long userId = PrincipalUtils.getUserIdFromPrincipal(principal);
		User user = userService.getUserById(userId);

		RegularRecord regularRecord = regularRecordFormConverter.convert(addingRegularRecordForm);
		regularRecord = regularRecordService.saveRegularRecord(user, regularRecord);
		log.info("Save regular record {}", regularRecord);

		return "redirect:/" + addingRegularRecordForm.getPageName();
	}

	@PostMapping("/save")
	public String saveRegularRecord(Principal principal, @ModelAttribute RegularRecordForm regularRecordForm) {

		RegularRecord regularRecord = regularRecordFormConverter.convert(regularRecordForm);
		long recordUserId = regularRecord.getUser().getId();
		long principalUserId = PrincipalUtils.getUserIdFromPrincipal(principal);

		if (Long.compare(recordUserId, principalUserId) != 0) {
			return "redirect:/records/regular1";
		}

		regularRecord = regularRecordService.saveRegularRecord(regularRecord);
		log.info("Save regular record {}", regularRecord);

		return "redirect:/records/regular/1";
	}

	@RequestMapping("/remove/{regularRecordId}")
	public String removeRegularRecord(Principal principal, Model model,
			@PathVariable("regularRecordId") long regularRecordId) {
		long principalUserId = PrincipalUtils.getUserIdFromPrincipal(principal);

		RegularRecord regularRecord = regularRecordService.getRegularRecordById(regularRecordId);
		if (regularRecord == null) {
			return "redirect:/records/regular/1";
		}

		long recordUserId = regularRecord.getUser().getId();
		if (Long.compare(principalUserId, recordUserId) != 0) {
			return "redirect:/records/regular/1";
		}

		regularRecordService.removeRegularRecord(regularRecord);
		return "redirect:/records/regular/1";
	}

	private void appendRegularRecords(Principal principal, Device device, Model model, int pageNumber) {
		long userId = getUserIdFromPrincipal(principal);

		int pageSize = PageSettingUtils.getPageSize(device);
		int total = regularRecordService.getRegularRecordsCountByUserId(userId);

		PageCondition pageCondition = new PageCondition();
		pageCondition.setPageNumber(pageNumber - 1);
		pageCondition.setPageSize(pageSize);

		List<RegularRecord> regularRecords = regularRecordService.findRegularRecordsByUserId(userId, pageCondition);
		List<RegularRecordView> regularRecordViews = regularRecordViewConverter.convert(regularRecords);
		Collections.sort(regularRecordViews);

		RegularRecordsView regularRecordsView = new RegularRecordsView();
		regularRecordsView.setRegularRecords(regularRecordViews);
		regularRecordsView.setTotal(total);
		regularRecordsView.setCurrentPage(pageNumber);
		regularRecordsView.setPageSize(pageSize);

		PageSettingUtils.appendPages(regularRecordsView, device, total, pageSize, pageNumber);

		model.addAttribute("regularRecords", regularRecordsView);
	}

	private void appendRecordTypes(Model model) {
		List<RecordType> recordTypes = recordTypeService.findRegularableRecordTypes();
		List<RecordTypeView> recordTypeViews = recordTypeViewConverter.convert(recordTypes);
		model.addAttribute("recordTypes", recordTypeViews);
	}

	private RegularRecordView createDefaultRegularRecordView() {
		RegularRecordView regularRecordView = new RegularRecordView();
		regularRecordView.setMoney(0);
		regularRecordView.setName(StringUtils.EMPTY);
		regularRecordView.setNote(StringUtils.EMPTY);

		regularRecordView.setToolTypeDisplayName(CASH.getDisplayName());
		regularRecordView.setToolTypeName(CASH.name());

		regularRecordView.setRecordTypeDisplayName(OUTGOING.getDisplayName());
		regularRecordView.setRecordTypeName(OUTGOING.name());

		return regularRecordView;
	}

}
