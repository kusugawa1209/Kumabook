$(function() {
	$('#recordDatePicker').datepicker({
		format : 'yyyy/mm/dd',
		defaultViewDate : getDateObject(),
		todayHighlight : true,
		autoclose : true
	}).on("changeDate", function(e) {
		$("#recordDate").val(e.date);
	});

	$(".recordType li a").on("click", function(e) {
		bindingDropdown(e, "recordType");
		toggleToolType(e);
	});

	$(".toolType li a").on("click", function(e) {
		bindingDropdown(e, "toolType");
		toggleLayaway(e);
	});

	$(".layaway li a").on("click", function(e) {
		bindingDropdown(e, "layaway");
	});

	$("#saveRecord").on("click", function() {
		submitRecord();
	});

	$("#recordItem").on("keydown", function(e) {
		var keyCode = e.keyCode;
		if (keyCode == ENTER_KEY_CODE) {
			$("#recordMoney").focus();
		}
	});

	$("#recordMoney").on("keydown", function(e) {
		var keyCode = e.keyCode;
		if (keyCode == TAB_KEY_CODE || keyCode == ENTER_KEY_CODE) {
			submitRecord();
		}
	});

	$("#recordMoney").on("focus", function(e) {
		var $target = $(e.target);
		var recordMoney = $target.val();
		if (recordMoney == 0) {
			$target.val('');
		}
	});

	$("#recordMoney").on("blur", function(e) {
		var $target = $(e.target);
		var recordMoney = $target.val();
		if (isNaN(parseInt(recordMoney))) {
			$target.val(0);
		}
	});

	function validateForm() {
		var messages = [];
		if ($("#recordItem").val().trim().length == 0) {
			messages.push(ITEM_NOT_EMPTY);
		}

		var recordMoneyValue = $("#recordMoney").val();
		var recordMoney = parseInt(recordMoneyValue);
		if (isNaN(recordMoney)) {
			messages.push(MONEY_INCORRECT);
		} else if (recordMoney <= 0) {
			messages.push(MONEY_INCORRECT);
		}

		return messages;
	}

	function submitRecord() {
		var errorMessages = validateForm();
		if (errorMessages.length > 0) {
			notify().error(errorMessages);
		} else {
			$("#record").submit();
		}
	}
});