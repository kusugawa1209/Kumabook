$(function() {
	$(".editCreditCard").on("click", function(e) {
		var $target = $(e.target);
		var creditCardId = $target.closest("button").attr("creditCardId");
		window.location.href = "/creditCards/editing/" + creditCardId;
	});

	$(".removeCreditCard").on("click", function(e) {
		var $target = $(e.target);
		var creditCardId = $target.closest("button").attr("creditCardId");
		window.location.href = "/creditCards/remove/" + creditCardId;
	});
});