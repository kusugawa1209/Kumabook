$(function() {
	adjustBodySize();
});

$(window).resize(function() {
	adjustBodySize();
});

function adjustBodySize() {
	$('body').height(
			$(window).height() - $('.navbar-fixed-top').outerHeight()
					- $('.navBottom').outerHeight());
}

var NONE = "NONE";
var CASH = "CASH";
var CREDIT = "CREDIT";
var TRANSFER = "TRANSFER";
var WITHDRAW = "WITHDRAW";
var SAVING_OUT = "SAVING_OUT";

var MONEY_REGEXP = /[0-9]+/;
var TAB_KEY_CODE = 9;
var ENTER_KEY_CODE = 13;

var NAME_NOT_EMPTY = "名稱不得為空。";
var ITEM_NOT_EMPTY = "項目不得為空。";

var MONEY_INCORRECT = "金額只能為正整數。";

var FROM_MONEY_BIGGER_THAN_TO_MONEY = "最低金額不能大於最高金額唷。";

var notify = function() {
	return {
		error : error,
		success : success
	};

	function error(message) {
		showMessage(message, "danger");
	}

	function success(message) {
		showMessage(message, "success");
	}

	function showMessage(message, type) {
		$.notify({
			message : handleMessage(message),
		}, {
			type : type,
			newest_on_top : true,
			placement : {
				from : "bottom",
				align : "center"
			},
			offset : {
				y : $(window).height() * 0.15
			},
			spacing : 10,
			z_index : 1031,
			delay : 2000,
			timer : 1000,
			mouse_over : "pause",
			animate : {
				enter : 'animated fadeInDown',
				exit : 'animated fadeOutUp'
			}
		});
	}

	function handleMessage(message) {
		if (message instanceof Array) {
			return message.join("<br />");
		} else {
			return message;
		}
	}
};

function getDateObject() {
	var today = new Date();

	return {
		year : today.getFullYear(),
		month : today.getMonth() + 1,
		date : today.getDate()
	}
}

function bindingDropdown(e, typeName) {
	var typeValue = $(e.target).attr("value");
	$("#" + typeName).val(typeValue);

	var typeDisplayName = $(e.target).text();
	$("#" + typeName + "s span").first().text(typeDisplayName);
}

function toggleToolType(e) {
	var $target = $(e.target);
	var value = $(e.target).attr("value");
	if (SAVING_OUT == value || WITHDRAW == value) {
		$("#toolType").val('TRANSFER');
		$(".toolType:not('hidden')").addClass("hidden");
	} else {
		$(".toolType").removeClass("hidden");
	}
}

function toggleLayaway(e) {
	var $target = $(e.target);
	var value = $(e.target).attr("value");
	if (CASH != value && TRANSFER != value && NONE != value) {
		$(".layaway").removeClass("hidden");
	} else {
		$("#layaway").val(1);
		$(".layaway:not('hidden')").addClass("hidden");
	}
}
