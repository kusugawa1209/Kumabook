$(function() {
	$('#fromDatePicker').datepicker({
		format : 'yyyy/mm/dd',
		todayHighlight : true,
		autoclose : true
	}).on("changeDate", function(e) {
		$("#fromDate").val(getFormatedDate(e.date));

		$("#toDatePicker").datepicker('setStartDate', e.date);
		if ($("#toDatePicker").datepicker('getDate') < e.date) {
			$("#toDatePicker").datepicker('setDate', e.date);
			$("#toDate").val(getFormatedDate(e.date));
		}
	});

	$('#toDatePicker').datepicker({
		format : 'yyyy/mm/dd',
		todayHighlight : true,
		autoclose : true
	}).on("changeDate", function(e) {
		$("#toDate").val(getFormatedDate(e.date));

		$("#fromDatePicker").datepicker('setEndDate', e.date);
		if ($("#fromDatePicker").datepicker('getDate') > e.date) {
			$("#fromDatePicker").datepicker('setDate', e.date);
			$("#fromDate").val(getFormatedDate(e.date));
		}
	});

	$(".recordType li a").on("click", function(e) {
		bindingDropdown(e, "recordType");
		toggleToolType(e);
	});

	$(".toolType li a").on("click", function(e) {
		bindingDropdown(e, "toolType");
	});

	$("#searchRecord").on("click", function() {
		searchRecord();
	});

	$("#recordItem").on("keydown", function(e) {
		var keyCode = e.keyCode;
		if (keyCode == ENTER_KEY_CODE) {
			$("#fromMoney").focus();
		}
	});

	$("#fromMoney").on("keydown", function(e) {
		var keyCode = e.keyCode;
		if (keyCode == ENTER_KEY_CODE) {
			$("#toMoney").focus();
		}
	});

	$("#fromMoney").on("focus", function(e) {
		var $target = $(e.target);
		var recordMoney = $target.val();
		if (recordMoney == 0) {
			$target.val('');
		}
	});

	$("#fromMoney").on("blur", function(e) {
		var $target = $(e.target);
		var recordMoney = $target.val();
		if (isNaN(parseInt(recordMoney))) {
			$target.val(0);
		}
	});

	$("#toMoney").on("keydown", function(e) {
		var keyCode = e.keyCode;
		if (keyCode == TAB_KEY_CODE || keyCode == ENTER_KEY_CODE) {
			submitRecord("home");
		}
	});

	$("#toMoney").on("focus", function(e) {
		var $target = $(e.target);
		var recordMoney = $target.val();
		if (recordMoney == 0) {
			$target.val('');
		}
	});

	$("#toMoney").on("blur", function(e) {
		var $target = $(e.target);
		var recordMoney = $target.val();
		if (isNaN(parseInt(recordMoney))) {
			$target.val(0);
		}
	});

	function searchRecord() {
		var fromMoney = $("#fromMoney").val();
		var toMoney = $("#toMoney").val();
		if (fromMoney && toMoney && Number(fromMoney) > Number(toMoney)) {
			notify().error(FROM_MONEY_BIGGER_THAN_TO_MONEY);
			return;
		}

		$.ajax({
			type : "POST",
			url : "/searching/record",
			headers : {
				'Content-Type' : 'application/x-www-form-urlencoded',
				'X-XSRF-TOKEN' : $.cookie('XSRF-TOKEN')
			},
			data : $("#searchForm").serialize()
		}).done(function(data) {
			console.log(data);
		}).fail(function(response) {
			console.log(response);
		}).always(function() {
			console.log('finally');
		});
	}
	
	function getFormatedDate(date) {
		return date.getFullYear() + "/" + (date.getMonth() + 1) + "/" + date.getDate();
	}
});