$(function() {
	$(".recordType li a").on("click", function(e) {
		bindingDropdown(e, "recordType");
		toggleToolType(e);
	});
	
	$(".toolType li a").on("click", function(e) {
		bindingDropdown(e, "toolType");
	});
	
	$("#saveRegularRecord").on("click", function() {
		submitRegularRecord("records/regular/1");
	});
	
	$("#addNext").on("click", function() {
		submitRegularRecord("records/regular/adding");
	});

	$("#recordItem").on("keydown", function(e) {
		var keyCode = e.keyCode;
		if (keyCode == ENTER_KEY_CODE) {
			$("#recordMoney").focus();
		}
	});
	
	$("#recordMoney").on("keydown", function (e) {
		var keyCode = e.keyCode;
		if (keyCode == ENTER_KEY_CODE) {
			$("#note").focus();
		}
	});

	$("#recordMoney").on("focus", function (e) {
		var $target = $(e.target);
		var recordMoney = $target.val();
		if(recordMoney == 0) {
			$target.val('');
		}
	});
	
	$("#recordMoney").on("blur", function (e) {
		var $target = $(e.target);
		var recordMoney = $target.val();
		if(isNaN(parseInt(recordMoney))) {
			$target.val(0);
		}
	});

	$("#note").on("keydown", function (e) {
		var keyCode = e.keyCode;
		if (keyCode == TAB_KEY_CODE || keyCode == ENTER_KEY_CODE) {
			submitRegularRecord("records/regular/1");
		}
	});
	
	function validateForm() {
		var messages = [];
		if ($("#recordItem").val().trim().length == 0) {
			messages.push(ITEM_NOT_EMPTY);
		}

		var recordMoneyValue = $("#recordMoney").val();
		var recordMoney = parseInt(recordMoneyValue);
		if (isNaN(recordMoney)) {
			messages.push(MONEY_INCORRECT);
		} else if (recordMoney <= 0) {
			messages.push(MONEY_INCORRECT);
		}

		return messages;
	}

	function submitRegularRecord(redirectTo) {
		var errorMessages = validateForm();
		if(errorMessages.length > 0) {
			notify().error(errorMessages);
		} else {
			$("#pageName").val(redirectTo);
			$("#regularRecord").submit();
		}
	}
});