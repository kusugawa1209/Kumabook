$(function() {
	$(".billingDay li a").on("click", function(e) {
		bindingDropdown(e, "billingDay");
	});

	$("#saveCreditCard").on("click", function() {
		submitCreditCard();
	});

	function validateForm() {
		var messages = [];
		if ($("#name").val().trim().length == 0) {
			messages.push(NAME_NOT_EMPTY);
		}

		return messages;
	}

	function submitCreditCard() {
		var errorMessages = validateForm();
		if (errorMessages.length > 0) {
			notify().error(errorMessages);
		} else {
			$("#creditCard").submit();
		}
	}
});