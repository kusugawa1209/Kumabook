angular.module("app", []).controller("home", function($http, $location) {
	var self = this;
	$http.get("/user", {
		headers : {
			"Content-Type" : "text/plain"
		}
	}).success(function(data) {
		self.user = data.username;
		self.authenticated = true;
		window.location.href = "/home";
	}).error(function() {
		self.user = "N/A";
		self.authenticated = false;

		window.location.href = "/login";
	});
});