$(function() {
	$(".billingDay li a").on("click", function(e) {
		bindingDropdown(e, "billingDay");
	});

	$("#addCreditCard").on("click", function() {
		submitCreditCard("creditCards/1");
	});

	$("#addNext").on("click", function() {
		submitCreditCard("creditCards/adding");
	});

	function validateForm() {
		var messages = [];
		if ($("#name").val().trim().length == 0) {
			messages.push(NAME_NOT_EMPTY);
		}

		return messages;
	}

	function submitCreditCard(redirectTo) {
		var errorMessages = validateForm();
		if (errorMessages.length > 0) {
			notify().error(errorMessages);
		} else {
			$("#pageName").val(redirectTo);
			$("#creditCard").submit();
		}
	}
});