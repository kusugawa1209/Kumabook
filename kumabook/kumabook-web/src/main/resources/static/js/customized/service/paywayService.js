app.service('paywayService', [ '$q', '$http', function($q, $http) {
	var self = this;

	self.findPayways = function() {
		var deferred = $q.defer();
		$http({
			method : 'GET',
			url : '/payway/all',
			cache : true
		}).then(function successCallback(response) {
			deferred.resolve(response.data);
		}, function errorCallback(response) {
			deferred.reject(response);
		});

		return deferred.promise;
	};
} ]);