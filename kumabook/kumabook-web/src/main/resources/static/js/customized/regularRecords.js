$(function() {
	$(".editRegularRecord").on("click", function(e) {
		var $target = $(e.target);
		var recordId = $target.closest("button").attr("recordId");
		window.location.href = "/records/regular/editing/" + recordId;
	});

	$(".removeRegularRecord").on("click", function(e) {
		var $target = $(e.target);
		var recordId = $target.closest("button").attr("recordId");
		window.location.href = "/records/regular/remove/" + recordId;
	});

	$(".regularRecord [data-toggle=confirmation]").confirmation({
		rootSelector : "[data-toggle=confirmation]",
		buttons : [ {
			class : "btn btn-default btn-lg",
			icon : "glyphicon glyphicon-trash",
			onClick : function() {
				var recordId = $(this).closest('.regularRecord').find('.recordId').val();
				window.location.href = "/records/regular/remove/" + recordId;
			}
		}, {
			class : "btn btn-default btn-lg",
			icon : "glyphicon glyphicon-pencil",
			onClick : function() {
				var recordId = $(this).closest('.regularRecord').find('.recordId').val();
				window.location.href = "/records/regular/editing/" + recordId;
			}
		}, {
			class : "btn btn-default btn-lg",
			icon : "glyphicon glyphicon-remove",
			cancel : true
		} ]
	});
});