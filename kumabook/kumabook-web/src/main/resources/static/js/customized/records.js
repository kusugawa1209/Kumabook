$(function() {
	$(".editRecord").on("click", function(e) {
		var $target = $(e.target);
		var recordId = $target.closest("button").attr("recordId");
		window.location.href = "/records/editing/" + recordId;
	});

	$(".removeRecord").on("click", function(e) {
		var $target = $(e.target);
		var recordId = $target.closest("button").attr("recordId");
		window.location.href = "/records/remove/" + recordId;
	});

	$(".record [data-toggle=confirmation]").confirmation({
		rootSelector : "[data-toggle=confirmation]",
		buttons : [ {
			class : "btn btn-default btn-lg",
			icon : "glyphicon glyphicon-trash",
			onClick : function() {
				var recordId = $(this).closest('.record').find('.recordId').val();
				window.location.href = "/records/remove/" + recordId;
			}
		}, {
			class : "btn btn-default btn-lg",
			icon : "glyphicon glyphicon-pencil",
			onClick : function() {
				var recordId = $(this).closest('.record').find('.recordId').val();
				window.location.href = "/records/editing/" + recordId;
			}
		}, {
			class : "btn btn-default btn-lg",
			icon : "glyphicon glyphicon-remove",
			cancel : true
		} ]
	});
});