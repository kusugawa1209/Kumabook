package com.kuma.kumabook.base;

import static java.util.Collections.emptyList;
import static org.apache.commons.collections.CollectionUtils.isEmpty;

import java.util.Collection;
import java.util.List;

import com.google.common.collect.Lists;
import com.kuma.kumabook.base.utils.ClassUtils2;

public interface GenericConverter<T> {

	/**
	 * convert source model to target model.
	 * 
	 * @param sources
	 *            - the collection of source models.
	 * 
	 * @return the list of target model
	 */
	public default List<T> convert(Collection<?> sources) {

		if (sources == null) {
			return emptyList();
		}

		if (isEmpty(sources)) {
			return emptyList();
		}

		List<T> targets = Lists.newArrayList();

		for (Object source : sources) {

			T target = null;

			if (source != null) {
				target = ClassUtils2.invokeMethod(this, "convert", source);
			}

			targets.add(target);
		}

		return targets;
	}

	/**
	 * convert source model to target model.
	 * 
	 * @param <E>
	 * 
	 * @param sources
	 *            - the collection of source models.
	 * 
	 * @return the list of target model
	 */
	public default <E> List<T> convert(E[] sources) {

		if (sources == null) {
			return emptyList();
		}

		if (sources.length == 0) {
			return emptyList();
		}

		List<T> targets = Lists.newArrayList();

		for (Object source : sources) {

			T target = null;

			if (source != null) {
				target = ClassUtils2.invokeMethod(this, "convert", source);
			}

			targets.add(target);
		}

		return targets;
	}

}
