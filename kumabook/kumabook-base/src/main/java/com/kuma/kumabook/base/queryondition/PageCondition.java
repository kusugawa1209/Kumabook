package com.kuma.kumabook.base.queryondition;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PageCondition {

	private int pageSize;

	private int pageNumber;
}
