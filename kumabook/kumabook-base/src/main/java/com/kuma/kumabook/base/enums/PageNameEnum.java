package com.kuma.kumabook.base.enums;

import lombok.Getter;

@Getter
public enum PageNameEnum {
	HOME("home"), RECORDS("records"), ADDING("records/adding");

	private String pageName;

	PageNameEnum(String pageName) {
		this.pageName = pageName;
	}
}
