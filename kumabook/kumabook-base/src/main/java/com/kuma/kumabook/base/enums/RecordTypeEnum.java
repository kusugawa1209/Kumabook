package com.kuma.kumabook.base.enums;

import static com.kuma.kumabook.base.enums.BudgetTypeEnum.IN;
import static com.kuma.kumabook.base.enums.BudgetTypeEnum.OUT;

import lombok.Getter;

public enum RecordTypeEnum {
	// @formatter:off
	OUTGOING("支出", OUT, true), INCOME("收入", IN, true), SAVING("存款", OUT, true), WITHDRAW("提款", IN, false);
	// @formatter:on

	@Getter
	private String displayName;

	@Getter
	private BudgetTypeEnum budgetType;

	@Getter
	private boolean regularable;

	RecordTypeEnum(String displayName, BudgetTypeEnum budgetType, boolean regularable) {
		this.displayName = displayName;
		this.budgetType = budgetType;
		this.regularable = regularable;
	}
}
