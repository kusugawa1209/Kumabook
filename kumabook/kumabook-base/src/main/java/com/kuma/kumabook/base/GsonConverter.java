package com.kuma.kumabook.base;

import static com.google.common.collect.Collections2.transform;
import static java.util.Collections.emptyList;
import static org.apache.commons.collections.CollectionUtils.isEmpty;

import java.util.Collection;
import java.util.List;
import java.util.Set;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public interface GsonConverter {

	public static <T> T convert(Object model, Class<T> clazz) {

		if (model == null) {
			return null;
		}

		String jsonString = toJsonString(model);
		return toObject(jsonString, clazz);
	}

	public static <T> List<T> convert(Collection<?> source, final Class<T> clazz) {

		if (isEmpty(source)) {
			return emptyList();
		}

		Iterable<T> elements = transform(source, (Object input) -> convert(input, clazz));

		return Lists.newArrayList(elements);
	}

	public static <T> List<T> convertToList(String jsonString, final Class<T> clazz) {
		List<?> objects = toObject(jsonString, List.class);

		return convert(objects, clazz);
	}

	public static <T> Set<T> convertToSet(String jsonString, final Class<T> clazz) {
		return Sets.newHashSet(convertToList(jsonString, clazz));
	}

	static String toJsonString(Object model) {
		return getGson().toJson(model);
	}

	static <T> T toObject(String jsonString, Class<T> clazz) {
		return getGson().fromJson(jsonString, clazz);
	}

	static Gson getGson() {
		return new GsonBuilder().create();
	}
}
