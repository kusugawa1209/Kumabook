package com.kuma.kumabook.base.excception;

public class IllegalInvocationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public IllegalInvocationException(String s) {
		super(s);
	}

	public IllegalInvocationException(String s, Throwable t) {
		super(s, t);
	}
}
