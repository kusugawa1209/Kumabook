package com.kuma.kumabook.base;

public final class LogMessageUtils {

	private LogMessageUtils() {

	}

	public static final String getSimpleClassName(Object object) {

		if (object instanceof Class) {
			return ((Class<?>) object).getSimpleName();
		}

		return object.getClass().getSimpleName();
	}

	public static final String getParameterNames(Class<?>[] parameterTypes) {

		StringBuilder result = new StringBuilder();

		for (Class<?> type : parameterTypes) {
			result.append(type.getSimpleName() + ", ");
		}

		int endIndex = result.length() <= 0 ? 0 : (result.length() - 2);
		return result.substring(0, endIndex);
	}

}
