package com.kuma.kumabook.base;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.PropertyMap;
import org.modelmapper.convention.MatchingStrategies;

import com.google.common.collect.Lists;

public abstract class GenericMapper<S, T extends Comparable<T>> {

	protected abstract Class<T> getTargetClass();

	public T mapping(S source) {
		if (source == null) {
			return null;
		}

		ModelMapper mapper = getMapper();
		return mapper.map(source, getTargetClass());
	}

	public List<T> mapping(Collection<S> sources) {
		List<T> targets = Lists.newArrayList();

		if (CollectionUtils.isEmpty(sources)) {
			return targets;
		}

		for (S source : sources) {
			T target = mapping(source);
			if (target != null) {
				targets.add(target);
			}
		}

		Collections.sort(targets);

		return targets;
	}

	private ModelMapper getMapper() {
		ModelMapper mapper = new ModelMapper();
		mapper.getConfiguration().setMatchingStrategy(MatchingStrategies.LOOSE);

		PropertyMap<S, T> propertyMap = getPropertyMap();
		if (propertyMap != null) {
			mapper.addMappings(propertyMap);
		}

		return mapper;
	}

	/**
	 * Explicit Mapping, 不需要就 return null
	 * 
	 * Example: PropertyMap<Order, OrderDTO> orderMap = new PropertyMap<Order,
	 * OrderDTO>() { protected void configure() {
	 * map().setBillingStreet(source.getBillingAddress().getStreet());
	 * map(source.billingAddress.getCity(), destination.billingCity); } };
	 * 
	 * @return
	 */
	protected PropertyMap<S, T> getPropertyMap() {
		return null;
	}
}
