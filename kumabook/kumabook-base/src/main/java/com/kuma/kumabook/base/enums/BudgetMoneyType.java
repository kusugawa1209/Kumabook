package com.kuma.kumabook.base.enums;

public enum BudgetMoneyType {
	CASH, CREDIT;
}
