package com.kuma.kumabook.base.enums;

import lombok.Getter;

public enum ToolTypeEnum {
	CASH("現金", false), CREDIT("信用卡", true), TRANSFER("轉帳", false);

	@Getter
	private String displayName;

	@Getter
	private boolean isCredit;

	ToolTypeEnum(String displayName, boolean isCredit) {
		this.displayName = displayName;
		this.isCredit = isCredit;
	}
}
