package com.kuma.kumabook.impl.service;

import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.kuma.kumabook.base.queryondition.PageCondition;
import com.kuma.kumabook.core.dao.RegularRecordDao;
import com.kuma.kumabook.core.model.RegularRecord;
import com.kuma.kumabook.core.model.User;
import com.kuma.kumabook.core.service.RegularRecordService;

@Service
@Transactional
public class RegularRecordServiceImpl implements RegularRecordService {

	@Autowired
	private RegularRecordDao regularRecordDao;

	@Override
	public RegularRecord getRegularRecordById(long regularRecordId) {
		return regularRecordDao.findOne(regularRecordId);
	}

	@Override
	public int getRegularRecordsCountByUserId(long userId) {
		Integer count = regularRecordDao.getCountByUserId(userId);

		if (count == null) {
			return 0;
		} else {
			return count.intValue();
		}
	}

	@Override
	public List<RegularRecord> findRegularRecordsByUserId(long userId) {
		return regularRecordDao.findByUserId(userId);
	}

	@Override
	public List<RegularRecord> findRegularRecordsByUserId(long userId, PageCondition pageCondition) {
		List<RegularRecord> regularRecords = regularRecordDao.findByUserId(userId);

		int pageSize = pageCondition.getPageSize();
		int pageNumber = pageCondition.getPageNumber();

		if (regularRecords.size() <= pageSize) {
			return regularRecords;
		}

		return Lists.partition(regularRecords, pageSize).get(pageNumber);
	}

	@Override
	public List<RegularRecord> findRegularRecordsByUserIdAndToolTypeName(long userId, String toolTypeName) {
		return regularRecordDao.findByUserIdAndToolTypeName(userId, toolTypeName);
	}

	@Override
	public RegularRecord saveRegularRecord(RegularRecord regularRecord) {
		return regularRecordDao.save(regularRecord);
	}

	@Override
	public RegularRecord saveRegularRecord(User user, RegularRecord regularRecord) {
		regularRecord.setUser(user);
		return saveRegularRecord(regularRecord);
	}

	@Override
	public List<RegularRecord> saveRegularRecords(Collection<RegularRecord> regularRecords) {
		return regularRecordDao.save(regularRecords);
	}

	@Override
	public void removeRegularRecord(RegularRecord regularRecord) {
		regularRecordDao.delete(regularRecord);
	}
}
