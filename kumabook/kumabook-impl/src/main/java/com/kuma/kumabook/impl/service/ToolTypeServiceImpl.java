package com.kuma.kumabook.impl.service;

import static com.kuma.kumabook.base.enums.ToolTypeEnum.CASH;
import static com.kuma.kumabook.base.enums.ToolTypeEnum.CREDIT;
import static com.kuma.kumabook.base.enums.ToolTypeEnum.TRANSFER;

import java.util.List;

import javax.transaction.Transactional;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.kuma.kumabook.core.model.CreditCard;
import com.kuma.kumabook.core.service.CreditCardService;
import com.kuma.kumabook.core.service.ToolTypeService;
import com.kuma.kumabook.core.vo.ToolType;

@Service
@Transactional
public class ToolTypeServiceImpl implements ToolTypeService {

	@Autowired
	private CreditCardService creditCardService;

	@Override
	public List<ToolType> findToolTypesByUserId(long userId) {
		List<ToolType> toolTypes = Lists.newArrayList();
		toolTypes.add(new ToolType(CASH.name(), CASH.getDisplayName()));
		toolTypes.add(new ToolType(TRANSFER.name(), TRANSFER.getDisplayName()));

		List<CreditCard> creditCards = creditCardService.findCreditCardsByUserId(userId);
		if (CollectionUtils.isEmpty(creditCards)) {
			toolTypes.add(new ToolType(CREDIT.name(), CREDIT.getDisplayName()));
		} else {
			for (CreditCard creditCard : creditCards) {
				String cardName = creditCard.getName();
				toolTypes.add(new ToolType(String.valueOf(creditCard.getId()), cardName));
			}
		}

		return toolTypes;
	}

}
