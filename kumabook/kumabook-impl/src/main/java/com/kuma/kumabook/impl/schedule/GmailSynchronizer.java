package com.kuma.kumabook.impl.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.kuma.kumabook.core.service.GmailService;

@Component
public class GmailSynchronizer {

	@Autowired
	private GmailService gmailService;

	@Async
	public void sync() {
		gmailService.retriveGmailRecords();
	}
}