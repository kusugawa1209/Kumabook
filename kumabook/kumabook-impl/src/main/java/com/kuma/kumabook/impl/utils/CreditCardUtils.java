package com.kuma.kumabook.impl.utils;

import static java.util.Calendar.DATE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Map;

import com.kuma.kumabook.core.model.Record;

public class CreditCardUtils {

	private CreditCardUtils() {

	}

	public static int getPassedMonth(Map<Long, Date> currentBillingDates, Record record) {
		String toolTypeName = record.getToolType();
		Long creditCardId = Long.valueOf(toolTypeName);

		Date currentBillingDate = currentBillingDates.get(creditCardId);

		GregorianCalendar creditCalendar = new GregorianCalendar();
		creditCalendar.setTime(currentBillingDate);
		creditCalendar.getTime();

		GregorianCalendar recordCalendar = new GregorianCalendar();
		recordCalendar.setTime(record.getDate());
		recordCalendar.getTime();

		int passedMonth = getPassedMonth(creditCalendar, recordCalendar);

		if (recordCalendar.get(DATE) >= creditCalendar.get(DATE)) {
			passedMonth -= 1;
		}

		return passedMonth;
	}

	public static int getPassedMonth(Calendar queryCalendar, Calendar recordCalendar) {
		int passedYear = queryCalendar.get(YEAR) - recordCalendar.get(YEAR);
		int passedMonth = queryCalendar.get(MONTH) - recordCalendar.get(MONTH);
		passedMonth += passedYear * 12;

		return passedMonth;
	}
}
