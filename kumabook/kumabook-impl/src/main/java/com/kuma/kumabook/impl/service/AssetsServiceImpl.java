package com.kuma.kumabook.impl.service;

import static com.kuma.kumabook.base.enums.RecordTypeEnum.INCOME;
import static com.kuma.kumabook.base.enums.RecordTypeEnum.OUTGOING;
import static com.kuma.kumabook.base.enums.RecordTypeEnum.SAVING;
import static com.kuma.kumabook.base.enums.RecordTypeEnum.WITHDRAW;
import static com.kuma.kumabook.base.enums.ToolTypeEnum.CASH;
import static com.kuma.kumabook.base.enums.ToolTypeEnum.TRANSFER;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuma.kumabook.base.enums.RecordTypeEnum;
import com.kuma.kumabook.core.model.Record;
import com.kuma.kumabook.core.service.AssetsService;
import com.kuma.kumabook.core.service.RecordService;
import com.kuma.kumabook.core.vo.Assets;

@Service
@Transactional
public class AssetsServiceImpl implements AssetsService {

	@Autowired
	private RecordService recordService;
	
	@Override
	public Assets getAssetsByUserId(long userId) {
		List<Record> records = recordService.findRecordsByUserId(userId);
		return convertRecordToAssets(records);
	}

	private Assets convertRecordToAssets(List<Record> records) {
		int in = 0;
		int out = 0;
		int saving = 0;
		for (Record record : records) {
			int recordMoney = record.getMoney();
			String recordTypeName = record.getRecordType();
			RecordTypeEnum recordType = RecordTypeEnum.valueOf(recordTypeName);

			String toolTypeName = record.getToolType();
			if (CASH.name().equals(toolTypeName) || TRANSFER.name().equals(toolTypeName)) {
				if (INCOME.equals(recordType)) {
					in += recordMoney;
				} else if (OUTGOING.equals(recordType)) {
					out += recordMoney;
				} else if (SAVING.equals(recordType)) {
					saving += recordMoney;
					out += recordMoney;
				} else if (WITHDRAW.equals(recordType)) {
					saving -= recordMoney;
					in += recordMoney;
				}
			}
		}

		Assets assets = new Assets();

		assets.setCash(in - out);
		assets.setSaving(saving);
		assets.setSum(in - out + saving);

		return assets;
	}
}
