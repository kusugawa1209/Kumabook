package com.kuma.kumabook.impl.service;

import static com.kuma.kumabook.base.enums.RecordTypeEnum.OUTGOING;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.mail.Flags;
import javax.mail.Folder;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.NoSuchProviderException;
import javax.mail.Session;
import javax.mail.Store;
import javax.mail.search.FlagTerm;
import javax.transaction.Transactional;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.kuma.kumabook.core.model.CreditCard;
import com.kuma.kumabook.core.model.GmailAccount;
import com.kuma.kumabook.core.model.Record;
import com.kuma.kumabook.core.model.User;
import com.kuma.kumabook.core.service.CreditCardService;
import com.kuma.kumabook.core.service.GmailService;
import com.kuma.kumabook.core.service.RecordService;
import com.kuma.kumabook.core.service.UserService;
import com.kuma.kumabook.core.vo.MailRecord;
import com.kuma.kumabook.mail.resolver.GmailMailResolver;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class GmailServiceImpl implements GmailService {

	@Autowired
	private UserService userService;

	@Autowired
	private RecordService recordService;

	@Autowired
	private CreditCardService creditCardService;

	@Autowired
	private GmailMailResolver gmailMailResolver;

	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");

	@Override
	public void retriveGmailRecords() {

		log.debug("Start retriving gmail records");

		Date now = new Date();

		List<User> users = userService.findUsers();
		for (User user : users) {
			GmailAccount gmailAccount = user.getGmailAccount();

			if (gmailAccount == null) {
				continue;
			}

			long userId = user.getId();
			List<MailRecord> mailRecords = findGmailRecords(userId, gmailAccount);

			if (CollectionUtils.isEmpty(mailRecords)) {
				continue;
			}

			List<Record> autoAddedRecords = recordService.findAutoAddedRecords(userId);

			for (MailRecord mailRecord : mailRecords) {

				boolean isMailRecordExisted = isMailRecordExisted(autoAddedRecords, mailRecord);
				if (isMailRecordExisted) {
					continue;
				}

				Record record = new Record();

				record.setCreationDate(now);
				record.setDate(mailRecord.getDate());
				record.setItemName(mailRecord.getItem());
				record.setLayaway(1);
				record.setModificationDate(now);
				record.setMoney(mailRecord.getMoney());
				record.setNote(mailRecord.getNote());
				record.setRecordType(OUTGOING.name());
				record.setToolType(mailRecord.getToolTypeName());
				record.setUser(user);
				record.setIsAutoAdded(true);

				recordService.saveRecord(record);

				log.info("Add new record: {}", record);
			}
		}
	}

	private boolean isMailRecordExisted(List<Record> autoAddedRecords, MailRecord mailRecord) {
		for (Record autoAddedRecord : autoAddedRecords) {
			String mailRecordDateValue = dateFormat.format(mailRecord.getDate());
			String autoAddedRecordDateValue = dateFormat.format(autoAddedRecord.getDate());
			if (!StringUtils.equals(mailRecordDateValue, autoAddedRecordDateValue)) {
				continue;
			}

			if (!StringUtils.equals(mailRecord.getItem(), autoAddedRecord.getItemName())) {
				continue;
			}

			if (Integer.compare(mailRecord.getMoney(), autoAddedRecord.getMoney()) != 0) {
				continue;
			}

			if (!StringUtils.equals(mailRecord.getNote(), autoAddedRecord.getNote())) {
				continue;
			}

			if (!StringUtils.equals(mailRecord.getToolTypeName(), autoAddedRecord.getToolType())) {
				continue;
			}

			return true;
		}

		return false;
	}

	public List<MailRecord> findGmailRecords(long userId, GmailAccount gmailAccount) {
		List<CreditCard> creditCards = creditCardService.findCreditCardsByUserId(userId);

		List<MailRecord> allMailRecords = Lists.newArrayList();

		Properties props = System.getProperties();
		props.setProperty("mail.store.protocol", "imaps");
		try {
			Session session = Session.getDefaultInstance(props, null);
			Store store = session.getStore("imaps");
			store.connect("imap.gmail.com", 993, gmailAccount.getUserAccount(), gmailAccount.getPassword());
			Folder inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_ONLY);
			Message[] unreadMessages = inbox.search(new FlagTerm(new Flags(Flags.Flag.SEEN), false));
			for (Message unreadMessage : unreadMessages) {

				List<MailRecord> mailRecords = gmailMailResolver.retriveMailRecordsFromUnreadMessage(creditCards,
						unreadMessage);

				if (CollectionUtils.isEmpty(mailRecords)) {
					continue;
				}

				allMailRecords.addAll(mailRecords);
			}
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}

		return allMailRecords;
	}
}
