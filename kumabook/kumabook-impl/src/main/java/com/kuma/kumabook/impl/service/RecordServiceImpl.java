package com.kuma.kumabook.impl.service;

import static java.util.Calendar.DATE;
import static java.util.Calendar.HOUR;
import static java.util.Calendar.MILLISECOND;
import static java.util.Calendar.MINUTE;
import static java.util.Calendar.SECOND;

import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.kuma.kumabook.base.queryondition.PageCondition;
import com.kuma.kumabook.core.dao.RecordDao;
import com.kuma.kumabook.core.model.Record;
import com.kuma.kumabook.core.model.User;
import com.kuma.kumabook.core.service.RecordService;
import com.kuma.kumabook.core.specification.RecordSearchCondition;

@Service
@Transactional
public class RecordServiceImpl implements RecordService {

	@Autowired
	private RecordDao recordDao;

	@Override
	public Record getRecordById(long recordId) {
		return recordDao.findOne(recordId);
	}

	@Override
	public List<Record> findRecordsByUserId(long userId) {
		return recordDao.findByUserId(userId);
	}

	@Override
	public List<Record> findTodayRecordsByUserId(long userId) {
		GregorianCalendar afterCalendar = new GregorianCalendar();
		afterCalendar.setTime(new Date());
		afterCalendar.set(DATE, afterCalendar.get(DATE) - 1);
		afterCalendar.set(HOUR, afterCalendar.getActualMaximum(HOUR));
		afterCalendar.set(MINUTE, afterCalendar.getActualMaximum(MINUTE));
		afterCalendar.set(SECOND, afterCalendar.getActualMaximum(SECOND));
		afterCalendar.set(MILLISECOND, afterCalendar.getActualMaximum(MILLISECOND));
		Date afterDate = afterCalendar.getTime();

		GregorianCalendar beforeCalendar = new GregorianCalendar();
		beforeCalendar.setTime(new Date());
		beforeCalendar.set(HOUR, beforeCalendar.getActualMaximum(HOUR));
		beforeCalendar.set(MINUTE, beforeCalendar.getActualMaximum(MINUTE));
		beforeCalendar.set(SECOND, beforeCalendar.getActualMaximum(SECOND));
		beforeCalendar.set(MILLISECOND, beforeCalendar.getActualMaximum(MILLISECOND));
		Date beforeDate = beforeCalendar.getTime();

		return recordDao.findNonCreditRecordsByUserIdAndDates(userId, afterDate, beforeDate);
	}
	
	@Override
	public List<Record> findMonthRecordsByUserId(long userId) {
		GregorianCalendar afterCalendar = new GregorianCalendar();
		afterCalendar.setTime(new Date());
		afterCalendar.set(DATE, afterCalendar.getActualMinimum(DATE));
		afterCalendar.set(HOUR, afterCalendar.getActualMinimum(HOUR));
		afterCalendar.set(MINUTE, afterCalendar.getActualMinimum(MINUTE));
		afterCalendar.set(SECOND, afterCalendar.getActualMinimum(SECOND));
		afterCalendar.set(MILLISECOND, afterCalendar.getActualMinimum(MILLISECOND));
		Date afterDate = afterCalendar.getTime();
		
		GregorianCalendar beforeCalendar = new GregorianCalendar();
		beforeCalendar.setTime(new Date());
		beforeCalendar.set(DATE, afterCalendar.getActualMaximum(DATE));
		beforeCalendar.set(HOUR, beforeCalendar.getActualMaximum(HOUR));
		beforeCalendar.set(MINUTE, beforeCalendar.getActualMaximum(MINUTE));
		beforeCalendar.set(SECOND, beforeCalendar.getActualMaximum(SECOND));
		beforeCalendar.set(MILLISECOND, beforeCalendar.getActualMaximum(MILLISECOND));
		Date beforeDate = beforeCalendar.getTime();
		
		return recordDao.findNonCreditRecordsByUserIdAndDates(userId, afterDate, beforeDate);
	}

	@Override
	public List<Record> findRecordsByUserId(long userId, PageCondition pageCondition) {
		List<Record> records = recordDao.findByUserId(userId);

		int pageSize = pageCondition.getPageSize();
		int pageNumber = pageCondition.getPageNumber();

		if (records.size() <= pageSize) {
			return records;
		}

		return Lists.partition(records, pageSize).get(pageNumber);
	}

	@Override
	public List<Record> findRecordsByUserIdAndToolTypeName(long userId, String toolTypeName) {
		return recordDao.findByUserIdAndToolTypeName(userId, toolTypeName);
	}

	@Override
	public int getRecordsCountByUserId(long userId) {
		Integer count = recordDao.getCountByUserId(userId);

		if (count == null) {
			return 0;
		} else {
			return count.intValue();
		}
	}

	@Override
	public List<Record> findAutoAddedRecords(long userId) {
		return recordDao.findAutoAddedRecords(userId);
	}

	@Override
	public Record saveRecord(User user, Record record) {
		record.setUser(user);
		return saveRecord(record);
	}

	@Override
	public Record saveRecord(Record record) {
		return recordDao.save(record);
	}

	@Override
	public List<Record> saveRecords(Collection<Record> records) {
		return recordDao.save(records);
	}

	@Override
	public void removeRecord(Record record) {
		recordDao.delete(record);
	}

	@Override
	public List<Record> findRecords(RecordSearchCondition specification) {
		return recordDao.findAll(specification.getSpecification());
	}

	@Override
	public List<Record> findNonCreditRecords(long userId) {
		return recordDao.findNonCreditRecordsByUserId(userId);
	}

	@Override
	public List<Record> findCreditRecords(long userId) {
		return recordDao.findCreditRecordsByUserId(userId);
	}
}
