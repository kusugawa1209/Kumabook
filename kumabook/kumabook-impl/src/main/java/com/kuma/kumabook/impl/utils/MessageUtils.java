package com.kuma.kumabook.impl.utils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.internet.MimeUtility;

import org.apache.commons.lang3.StringUtils;

public class MessageUtils {

	public static String getSender(Message message) {
		try {
			String sender = message.getFrom()[0].toString();
			sender = MimeUtility.decodeText(sender);
			return sender;
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return StringUtils.EMPTY;
	}

	public static String getSubject(Message message) {
		try {
			return message.getSubject();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return StringUtils.EMPTY;
	}

	public static String getContent(Message message) {
		try {
			Object contentObject = message.getContent();

			return contentObject.toString();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return StringUtils.EMPTY;
	}
}
