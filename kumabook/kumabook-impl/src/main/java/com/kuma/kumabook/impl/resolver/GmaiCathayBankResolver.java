package com.kuma.kumabook.impl.resolver;

import static com.kuma.kumabook.base.enums.ToolTypeEnum.CREDIT;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.mail.Message;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;

import com.google.common.base.CharMatcher;
import com.google.common.collect.Lists;
import com.kuma.kumabook.core.model.CreditCard;
import com.kuma.kumabook.core.vo.MailRecord;
import com.kuma.kumabook.impl.utils.MessageUtils;
import com.kuma.kumabook.mail.resolver.GmailMailResolver;

@Component
public class GmaiCathayBankResolver implements GmailMailResolver {

	private static final String PARSING_TARGET = "國泰世華";

	private static final List<String> SENDERS = Lists.newArrayList("國泰世華銀行", "cathaybk");

	private static final String SUBJECT = "國泰世華銀行消費彙整通知";

	private static final String RECORD_PATTERN = "(\\d{4}/\\d{2}/\\d{2}).*\\r\\n.*(\\d{2}\\:\\d{2})"
			+ ".*\\r\\n.*>([a-zA-Z]{2}).*\\r\\n.*>\\w+\\$([0-9,]+)<.*\\r\\n.*>(\\S+)<.*\\r\\n.*>(\\S+)<";

	private static final String DATE_PATTERN = "yyyy/MM/dd";

	private static final String ITEM_PATTERN = "%s(%s)";

	@Override
	public List<MailRecord> retriveMailRecordsFromUnreadMessage(List<CreditCard> creditCards, Message message) {
		List<MailRecord> mailRecords = Lists.newArrayList();

		try {

			String subject = MessageUtils.getSubject(message);

			if (!StringUtils.contains(subject, SUBJECT)) {
				return null;
			}

			String sender = MessageUtils.getSender(message);
			boolean isSenderExist = isSenderExist(sender);
			if (!isSenderExist) {
				return null;
			}

			SimpleDateFormat dateFormat = new SimpleDateFormat(DATE_PATTERN);

			String content = MessageUtils.getContent(message);
			Pattern pattern = Pattern.compile(RECORD_PATTERN);
			Matcher matcher = pattern.matcher(content);
			while (matcher.find()) {
				String dateValue = matcher.group(1);
				String moneyValue = matcher.group(4);
				String storeNameValue = matcher.group(5);
				String category = matcher.group(6);

				MailRecord mailRecord = new MailRecord();

				Date date = dateFormat.parse(dateValue);
				mailRecord.setDate(date);

				String item = String.format(ITEM_PATTERN, storeNameValue, category);
				item = StringEscapeUtils.unescapeHtml4(item);
				item = CharMatcher.WHITESPACE.trimFrom(item);
				mailRecord.setItem(item);

				moneyValue = StringUtils.replace(moneyValue, ",", StringUtils.EMPTY);
				Double money = Double.parseDouble(moneyValue);
				mailRecord.setMoney(money.intValue());

				mailRecord.setNote("Add by " + this.getClass().getSimpleName() + ".");

				String toolTypeName = getToolTypeName(creditCards);
				mailRecord.setToolTypeName(toolTypeName);

				mailRecords.add(mailRecord);
			}

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return mailRecords;
	}

	private boolean isSenderExist(String sender) {

		for (String SENDER : SENDERS) {
			if (!StringUtils.contains(sender, SENDER)) {
				return false;
			}
		}

		return true;
	}

	private String getToolTypeName(List<CreditCard> creditCards) {
		for (CreditCard creditCard : creditCards) {
			String creditCardName = creditCard.getName();

			if (StringUtils.contains(creditCardName, PARSING_TARGET)
					|| StringUtils.contains(PARSING_TARGET, creditCardName)) {
				return String.valueOf(creditCard.getId());
			}
		}

		return CREDIT.name();
	}
}
