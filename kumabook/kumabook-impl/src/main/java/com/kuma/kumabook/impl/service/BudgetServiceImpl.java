package com.kuma.kumabook.impl.service;

import static com.kuma.kumabook.base.enums.BudgetMoneyType.CASH;
import static com.kuma.kumabook.base.enums.BudgetMoneyType.CREDIT;
import static com.kuma.kumabook.base.enums.RecordTypeEnum.INCOME;
import static com.kuma.kumabook.base.enums.RecordTypeEnum.OUTGOING;
import static com.kuma.kumabook.base.enums.RecordTypeEnum.SAVING;
import static com.kuma.kumabook.base.enums.RecordTypeEnum.WITHDRAW;
import static com.kuma.kumabook.base.enums.ToolTypeEnum.TRANSFER;
import static java.util.Calendar.DATE;
import static java.util.Calendar.MONTH;
import static java.util.Calendar.YEAR;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.transaction.Transactional;

import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Table;
import com.kuma.kumabook.base.enums.BudgetMoneyType;
import com.kuma.kumabook.base.enums.RecordTypeEnum;
import com.kuma.kumabook.base.enums.ToolTypeEnum;
import com.kuma.kumabook.core.model.Record;
import com.kuma.kumabook.core.model.RegularRecord;
import com.kuma.kumabook.core.service.BudgetService;
import com.kuma.kumabook.core.service.CreditCardService;
import com.kuma.kumabook.core.service.RecordService;
import com.kuma.kumabook.core.service.RegularRecordService;
import com.kuma.kumabook.core.vo.Budget;
import com.kuma.kumabook.core.vo.BudgetMoney;

@Service
@Transactional
public class BudgetServiceImpl implements BudgetService {

	@Autowired
	private RecordService recordService;

	@Autowired
	private RegularRecordService regularRecordService;

	@Autowired
	private CreditCardService creditCardService;

	@Override
	public Budget getTodayBudgetByUserId(long userId) {
		List<Record> records = recordService.findTodayRecordsByUserId(userId);

		Budget budget = new Budget();
		for (Record record : records) {
			int recordMoney = record.getMoney();
			RecordTypeEnum recordType = getRecordType(record);

			String toolTypeName = record.getToolType();
			boolean isNonCredit = CASH.name().equals(toolTypeName) || TRANSFER.name().equals(toolTypeName);

			if (!isNonCredit) {
				continue;
			}

			if (INCOME.equals(recordType)) {
				budget.getIncome().addMoney(CASH, recordMoney);
			} else if (OUTGOING.equals(recordType)) {
				budget.getOutgoing().addMoney(CASH, recordMoney);
			} else if (SAVING.equals(recordType)) {
				budget.getSaving().addMoney(CASH, recordMoney);
			} else if (WITHDRAW.equals(recordType)) {
				budget.getWithdraw().addMoney(CASH, recordMoney);
			}
		}

		budget.setSaving(budget.getSaving().minus(budget.getWithdraw()));
		calculateRemain(budget);

		return budget;
	}

	@Override
	public Budget getMonthlyBudgetByUserId(long userId) {
		List<Record> records = recordService.findTodayRecordsByUserId(userId);

		Budget budget = new Budget();
		for (Record record : records) {
			int recordMoney = record.getMoney();
			RecordTypeEnum recordType = getRecordType(record);

			if (INCOME.equals(recordType)) {
				budget.getIncome().addMoney(CASH, recordMoney);
			} else if (OUTGOING.equals(recordType)) {
				budget.getOutgoing().addMoney(CASH, recordMoney);
			} else if (SAVING.equals(recordType)) {
				budget.getSaving().addMoney(CASH, recordMoney);
			} else if (WITHDRAW.equals(recordType)) {
				budget.getWithdraw().addMoney(CASH, recordMoney);
			}
		}

		budget.setSaving(budget.getSaving().minus(budget.getWithdraw()));
		calculateRemain(budget);

		return budget;
	}

	@Override
	public List<Budget> findAllBudgets(long userId, int predictMonthCount) {
		Table<Integer, Integer, Budget> budgetTable = findBudgetTable(userId, predictMonthCount);

		List<Budget> budgets = getSortedBudgets(budgetTable);
		appendRegularRecords(userId, budgets);
		appendCreditRecords(userId, budgets);

		List<Budget> mergedBudgets = getMergedBudgets(budgets);

		return findCurrentBudgets(mergedBudgets);
	}

	private List<Budget> findCurrentBudgets(List<Budget> mergedBudgets) {
		int currentIndex = -1;
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());

		int currentYear = calendar.get(YEAR);
		int currentMonth = calendar.get(MONTH);

		for (Budget mergedBudget : mergedBudgets) {
			if (mergedBudget.getYear() == currentYear && mergedBudget.getMonth() == currentMonth) {
				currentIndex = mergedBudgets.indexOf(mergedBudget);
				break;
			}
		}

		if (currentIndex == -1) {
			return mergedBudgets;
		} else {
			return mergedBudgets.subList(currentIndex, mergedBudgets.size());
		}
	}

	private void calculateRemain(Budget budget) {
		budget.setRemain(budget.getIncome().minus(budget.getOutgoing()).minus(budget.getSaving()));
	}

	private void appendBudget(Budget budget, RecordTypeEnum recordType, int money, BudgetMoneyType moneyType) {
		if (INCOME.equals(recordType)) {
			budget.getIncome().addMoney(moneyType, money);
		} else if (OUTGOING.equals(recordType)) {
			budget.getOutgoing().addMoney(moneyType, money);
		} else if (SAVING.equals(recordType)) {
			budget.getSaving().addMoney(moneyType, money);
		} else if (WITHDRAW.equals(recordType)) {
			budget.getWithdraw().addMoney(moneyType, money);
		}
	}

	private Long getCreditCardId(String toolTypeName) {
		try {
			ToolTypeEnum.valueOf(toolTypeName);
			return null;
		} catch (Exception e) {
			return Long.valueOf(toolTypeName);
		}
	}

	private ToolTypeEnum getToolType(String toolTypeName) {
		try {
			return ToolTypeEnum.valueOf(toolTypeName);
		} catch (Exception e) {
			return ToolTypeEnum.CREDIT;
		}
	}

	private BudgetMoneyType getBudgetMoneyTypeByToolTypeName(String toolTypeName) {
		ToolTypeEnum toolType = getToolType(toolTypeName);
		return ToolTypeEnum.CREDIT.equals(toolType) ? CREDIT : CASH;
	}

	private void appendCreditRecords(long userId, List<Budget> budgets) {
		Calendar currentCalendar = Calendar.getInstance();
		currentCalendar.setTime(new Date());

		Map<Long, Integer> billingDays = creditCardService.getBillingDaysByUserId(userId);

		List<Record> creditRecords = recordService.findCreditRecords(userId);
		for (Record creditRecord : creditRecords) {
			Calendar recordCalendar = Calendar.getInstance();

			Date recordDate = creditRecord.getDate();
			recordCalendar.setTime(recordDate);

			int recordYear = recordCalendar.get(YEAR);
			int recordMonth = recordCalendar.get(MONTH);
			int recordDay = recordCalendar.get(DATE);

			int currentYear = currentCalendar.get(YEAR);
			int currentMonth = currentCalendar.get(MONTH);

			Long creditCardId = getCreditCardId(creditRecord);

			RecordTypeEnum recordType = getRecordType(creditRecord);

			int recordMoney = creditRecord.getMoney();

			int layaway = creditRecord.getLayaway();

			int targetMonth = getTargetMonth(billingDays, recordMonth, recordDay, creditCardId, recordType);

			int passedMonth = (recordYear - currentYear) * 12 + (targetMonth - currentMonth);
			if ((passedMonth <= 0 && layaway == 1) || (passedMonth <= 0 || passedMonth > layaway && layaway > 1)) {
				continue;
			}

			appendLayawayRecord(budgets, recordYear, targetMonth, recordType, recordMoney, layaway);
		}
	}

	private void appendLayawayRecord(List<Budget> budgets, int recordYear, int targetMonth, RecordTypeEnum recordType,
			int recordMoney, int layaway) {
		List<Integer> targetMonths = getTargetMonths(layaway, targetMonth);
		for (Budget budget : budgets) {
			for (Integer currentTargetMonth : targetMonths) {
				if (budget.getYear() == recordYear && budget.getMonth() == currentTargetMonth) {
					budget.addMoney(CREDIT, recordType, recordMoney / layaway);
				}
			}
		}
	}

	private int getTargetMonth(Map<Long, Integer> billingDays, int recordMonth, int recordDay, Long creditCardId,
			RecordTypeEnum recordType) {
		int targetMonth = recordMonth + 1;

		if (INCOME.equals(recordType)) {
			targetMonth -= 1;
		}

		if (creditCardId != null) {
			Integer billingDay = billingDays.get(creditCardId);

			if (recordDay >= billingDay) {
				targetMonth += 1;
			}
		}
		return targetMonth;
	}

	private Long getCreditCardId(Record creditRecord) {
		String toolTypeName = creditRecord.getToolType();
		return getCreditCardId(toolTypeName);
	}

	private RecordTypeEnum getRecordType(Record creditRecord) {
		String recordTypeName = creditRecord.getRecordType();
		return RecordTypeEnum.valueOf(recordTypeName);
	}

	private List<Integer> getTargetMonths(int layaway, int targetMonth) {
		List<Integer> targetMonths = Lists.newArrayList();
		if (layaway == 1) {
			targetMonths.add(targetMonth);
		} else {
			for (int i = targetMonth; i <= targetMonth + layaway; i++) {
				targetMonths.add(i);
			}
		}
		return targetMonths;
	}

	private void appendRegularRecords(long userId, List<Budget> budgets) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());

		List<RegularRecord> regularRecords = regularRecordService.findRegularRecordsByUserId(userId);

		int currentYear = calendar.get(YEAR);
		int currentMonth = calendar.get(MONTH);
		int currentDate = calendar.get(DATE);

		for (Budget budget : budgets) {
			int year = budget.getYear();
			int month = budget.getMonth();

			int passedMonth = (year - currentYear) * 12 + month - currentMonth;

			if (passedMonth > 0) {
				appendRegularRecords(budget, passedMonth, currentDate, regularRecords);
			}
		}
	}

	private void appendRegularRecords(Budget budget, int passedMonth, int currentDate,
			List<RegularRecord> regularRecords) {
		for (RegularRecord regularRecord : regularRecords) {
			String toolTypeName = regularRecord.getToolType();
			ToolTypeEnum toolType = getToolType(toolTypeName);

			int regularRecordMoney = regularRecord.getMoney();
			String recordTypeName = regularRecord.getRecordType();
			RecordTypeEnum recordType = RecordTypeEnum.valueOf(recordTypeName);

			BudgetMoneyType moneyType = getBudgetMoneyTypeByToolTypeName(toolTypeName);
			boolean isNonCredit = !toolType.isCredit();
			boolean isNextMonth = toolType.isCredit() && passedMonth == 1 && currentDate < 15;
			boolean isFuture = toolType.isCredit() && passedMonth > 1;
			if (isNonCredit || isNextMonth || isFuture) {
				budget.addMoney(moneyType, recordType, regularRecordMoney);
			}
		}
	}

	private List<Budget> getMergedBudgets(List<Budget> sortedBudgets) {
		List<Budget> mergedBudgets = Lists.newArrayList();

		for (Budget currentBudget : sortedBudgets) {
			currentBudget.setSaving(currentBudget.getSaving().minus(currentBudget.getWithdraw()));

			BudgetMoney remain = new BudgetMoney().add(currentBudget.getIncome()).minus(currentBudget.getOutgoing())
					.minus(currentBudget.getSaving());

			int index = sortedBudgets.indexOf(currentBudget);

			if (index > 0) {
				Budget prevBudget = mergedBudgets.get(index - 1);
				remain = remain.add(prevBudget.getRemain());

				currentBudget.setIncome(currentBudget.getIncome().add(prevBudget.getRemain()));
			}

			currentBudget.setRemain(remain);

			int percent = 0;
			if (currentBudget.getIncome().getSum() != 0) {
				percent = (currentBudget.getOutgoing().getSum() + currentBudget.getSaving().getSum()) * 100
						/ currentBudget.getIncome().getSum();
			}

			currentBudget.setPercent(percent);

			if (index > 0) {
				Budget prevBudget = mergedBudgets.get(index - 1);
				currentBudget.setSaving(currentBudget.getSaving().add(prevBudget.getSaving()));
			}

			mergedBudgets.add(currentBudget);
		}

		return mergedBudgets;
	}

	private List<Budget> getSortedBudgets(Table<Integer, Integer, Budget> budgets) {
		List<Budget> sortedBudgets = Lists.newArrayList(budgets.values());
		sortedBudgets.sort((Budget o1, Budget o2) -> {
			Integer o1Year = o1.getYear();
			Integer o2Year = o2.getYear();
			Integer o1Month = o1.getMonth();
			Integer o2Month = o2.getMonth();

			if (o1Year != o2Year) {
				return Integer.compare(o1Year, o2Year);
			} else {
				return Integer.compare(o1Month, o2Month);
			}
		});

		return sortedBudgets;
	}

	private Table<Integer, Integer, Budget> findBudgetTable(long userId, int predictMonthCount) {
		Table<Integer, Integer, Budget> budgets = HashBasedTable.create();

		List<Record> records = recordService.findNonCreditRecords(userId);

		for (Record record : records) {
			RecordTypeEnum recordType = getRecordType(record);

			int recordMoney = record.getMoney();

			Calendar calendar = Calendar.getInstance();
			Date date = record.getDate();
			calendar.setTime(date);

			int year = calendar.get(YEAR);
			int month = calendar.get(MONTH);

			Budget currentBudget = new Budget();
			Budget existedBudget = budgets.get(year, month);

			if (existedBudget != null) {
				currentBudget = existedBudget;
			}

			appendBudget(currentBudget, recordType, recordMoney, CASH);

			currentBudget.setYear(year);
			currentBudget.setMonth(month);
			budgets.put(year, month, currentBudget);
		}

		appendPredicatMonthBudgets(budgets, records, predictMonthCount);

		return budgets;
	}

	private void appendPredicatMonthBudgets(Table<Integer, Integer, Budget> budgets, List<Record> records,
			int predictMonthCount) {
		if (CollectionUtils.isEmpty(records)) {
			return;
		}

		Record lastRecord = records.get(records.size() - 1);
		for (int i = 1; i <= predictMonthCount; i++) {
			Calendar calendar = Calendar.getInstance();
			calendar.setTime(lastRecord.getDate());
			calendar.set(DATE, 1);
			calendar.set(MONTH, calendar.get(MONTH) + i);

			int newYear = calendar.get(YEAR);
			int newMonth = calendar.get(MONTH);

			Budget newBudget = new Budget();

			newBudget.setYear(newYear);
			newBudget.setMonth(newMonth);

			budgets.put(newYear, newMonth, newBudget);
		}
	}
}
