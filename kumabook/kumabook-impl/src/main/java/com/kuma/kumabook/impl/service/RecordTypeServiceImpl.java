package com.kuma.kumabook.impl.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.kuma.kumabook.base.enums.RecordTypeEnum;
import com.kuma.kumabook.core.service.RecordTypeService;
import com.kuma.kumabook.core.vo.RecordType;

@Service
public class RecordTypeServiceImpl implements RecordTypeService {

	@Override
	public List<RecordType> findAll() {
		List<RecordType> recordTypes = Lists.newArrayList();
		RecordTypeEnum[] recordTypeEnums = RecordTypeEnum.values();
		for (RecordTypeEnum recordTypeEnum : recordTypeEnums) {
			recordTypes.add(new RecordType(recordTypeEnum));
		}

		return recordTypes;
	}

	@Override
	public List<RecordType> findRegularableRecordTypes() {
		List<RecordType> recordTypes = Lists.newArrayList();
		RecordTypeEnum[] recordTypeEnums = RecordTypeEnum.values();
		for (RecordTypeEnum recordTypeEnum : recordTypeEnums) {
			if (recordTypeEnum.isRegularable()) {
				recordTypes.add(new RecordType(recordTypeEnum));
			}
		}
		
		return recordTypes;
	}

}
