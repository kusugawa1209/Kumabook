package com.kuma.kumabook.impl.service;

import static com.kuma.kumabook.base.enums.ToolTypeEnum.CREDIT;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.kuma.kumabook.base.queryondition.PageCondition;
import com.kuma.kumabook.core.dao.CreditCardDao;
import com.kuma.kumabook.core.model.CreditCard;
import com.kuma.kumabook.core.model.Record;
import com.kuma.kumabook.core.model.RegularRecord;
import com.kuma.kumabook.core.model.User;
import com.kuma.kumabook.core.service.CreditCardService;
import com.kuma.kumabook.core.service.RecordService;
import com.kuma.kumabook.core.service.RegularRecordService;

@Service
@Transactional
public class CreditCardServiceImpl implements CreditCardService {

	@Autowired
	private CreditCardDao creditCardDao;

	@Autowired
	private RecordService recordService;

	@Autowired
	private RegularRecordService regularRecordService;

	@Override
	public Map<Long, Date> getCurrentBillingDatesByUserId(long userId, Date currentDate) {
		GregorianCalendar calendar = new GregorianCalendar();
		calendar.setTime(currentDate);
		calendar.getTime();

		Map<Long, Date> billingDates = Maps.newHashMap();
		Map<Long, Integer> billingDays = getBillingDaysByUserId(userId);
		Set<Entry<Long, Integer>> billingDayEntries = billingDays.entrySet();
		for (Entry<Long, Integer> billingDayEntry : billingDayEntries) {
			calendar.set(Calendar.DATE, billingDayEntry.getValue());
			billingDates.put(billingDayEntry.getKey(), calendar.getTime());
		}

		return billingDates;
	}

	@Override
	public Map<Long, Integer> getBillingDaysByUserId(long userId) {
		Map<Long, Integer> billingDays = Maps.newHashMap();

		List<CreditCard> cards = findCreditCardsByUserId(userId);
		for (CreditCard card : cards) {
			billingDays.put(card.getId(), card.getBillingDay());
		}

		return billingDays;
	}

	@Override
	public CreditCard getCreditCardById(long cardId) {
		return creditCardDao.findOne(cardId);
	}

	@Override
	public List<CreditCard> findCreditCardsByUserId(long userId) {
		return creditCardDao.findByUserId(userId);
	}

	@Override
	public List<CreditCard> findCreditCardsByUserId(long userId, PageCondition pageCondition) {
		List<CreditCard> creditCards = creditCardDao.findByUserId(userId);

		int pageSize = pageCondition.getPageSize();
		int pageNumber = pageCondition.getPageNumber();

		if (creditCards.size() <= pageSize) {
			return creditCards;
		}

		return Lists.partition(creditCards, pageSize).get(pageNumber);
	}

	@Override
	public int getCreditCardsCountByUserId(long userId) {
		Integer count = creditCardDao.getCountByUserId(userId);
		return count == null ? 0 : count.intValue();
	}

	@Override
	public CreditCard saveCreditCard(User user, CreditCard creditCard) {
		creditCard.setUser(user);
		return saveCreditCard(creditCard);
	}

	@Override
	public CreditCard saveCreditCard(CreditCard creditCard) {
		return creditCardDao.save(creditCard);
	}

	@Override
	public void removeCreditCard(CreditCard creditCard) {
		long userId = creditCard.getUser().getId();
		Long cardId = creditCard.getId();
		String cardToolTypeName = String.valueOf(cardId);

		List<Record> records = recordService.findRecordsByUserIdAndToolTypeName(userId, cardToolTypeName);
		for (Record record : records) {
			record.setToolType(CREDIT.name());
		}
		recordService.saveRecords(records);

		List<RegularRecord> regularRecords = regularRecordService.findRegularRecordsByUserIdAndToolTypeName(userId,
				cardToolTypeName);
		for (RegularRecord regularRecord : regularRecords) {
			regularRecord.setToolType(CREDIT.name());
		}
		regularRecordService.saveRegularRecords(regularRecords);

		creditCardDao.delete(creditCard);
	}
}
