package com.kuma.kumabook.impl.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kuma.kumabook.core.dao.UserDao;
import com.kuma.kumabook.core.model.User;
import com.kuma.kumabook.core.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Override
	public List<User> findUsers() {
		return userDao.findAll();
	}

	@Override
	public User getUserById(long id) {
		return userDao.findOne(id);
	}

	@Override
	public User createAndSaveUser(long id, String username) {
		User user = new User();
		user.setId(id);
		user.setUsername(username);

		user = userDao.save(user);
		log.info("Create user {}({})", username, id);
		
		return user;
	}

	@Override
	public User saveUser(User user) {
		return userDao.save(user);
	}
}
