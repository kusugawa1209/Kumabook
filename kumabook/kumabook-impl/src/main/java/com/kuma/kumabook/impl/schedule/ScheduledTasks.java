package com.kuma.kumabook.impl.schedule;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class ScheduledTasks {

	@Autowired
	private GmailSynchronizer synchronizer;

	@Scheduled(fixedDelayString = "${schedule.mail.interval}")
	public void syncGmailRecords() {
		synchronizer.sync();
	}
}
