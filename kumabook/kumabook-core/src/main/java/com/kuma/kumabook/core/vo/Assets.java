package com.kuma.kumabook.core.vo;

import java.io.Serializable;

import lombok.Data;

@Data
public class Assets implements Serializable {

	private static final long serialVersionUID = 1L;

	private int cash = 0;
	
	private int saving = 0;
	
	private int sum = 0;
}
