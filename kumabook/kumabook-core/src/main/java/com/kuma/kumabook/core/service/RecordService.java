package com.kuma.kumabook.core.service;

import java.util.Collection;
import java.util.List;

import com.kuma.kumabook.base.queryondition.PageCondition;
import com.kuma.kumabook.core.model.Record;
import com.kuma.kumabook.core.model.User;
import com.kuma.kumabook.core.specification.RecordSearchCondition;

public interface RecordService {

	public Record getRecordById(long recordId);

	public List<Record> findRecordsByUserId(long userId);

	public List<Record> findTodayRecordsByUserId(long userId);
	
	public List<Record> findMonthRecordsByUserId(long userId);

	public List<Record> findRecordsByUserId(long userId, PageCondition pageCondition);

	public List<Record> findRecordsByUserIdAndToolTypeName(long userId, String toolTypeName);

	public int getRecordsCountByUserId(long userId);

	public List<Record> findAutoAddedRecords(long userId);

	public Record saveRecord(User user, Record record);

	public Record saveRecord(Record record);

	public List<Record> saveRecords(Collection<Record> records);

	public void removeRecord(Record record);

	public List<Record> findRecords(RecordSearchCondition specification);

	public List<Record> findNonCreditRecords(long userId);

	public List<Record> findCreditRecords(long userId);

}