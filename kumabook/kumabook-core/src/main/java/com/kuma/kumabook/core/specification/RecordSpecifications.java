package com.kuma.kumabook.core.specification;

import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import com.kuma.kumabook.core.model.Record;
import com.kuma.kumabook.core.model.Record_;
import com.kuma.kumabook.core.model.User_;

public class RecordSpecifications {

	private RecordSpecifications() {

	}

	public static Specification<Record> alwaysTrue() {
		return (Root<Record> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> cb.conjunction();
	}

	public static Specification<Record> equalUserId(final Long userId) {
		return (Root<Record> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> cb
				.equal(root.get(Record_.user).get(User_.id), userId);
	}

	public static Specification<Record> equalToolTypeName(final String toolTypeName) {
		return (Root<Record> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> cb.equal(root.get(Record_.toolType),
				toolTypeName);
	}

	public static Specification<Record> equalRecordTypeName(final String recordTypeName) {
		return (Root<Record> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> cb.equal(root.get(Record_.recordType),
				recordTypeName);
	}

	public static Specification<Record> likeName(final String name) {
		return (Root<Record> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> cb.like(root.get(Record_.itemName),
				"%" + name + "%");
	}

	public static Specification<Record> greaterThanOrEqualToStartDate(final Date startDate) {
		return (Root<Record> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> cb
				.greaterThanOrEqualTo(root.<Date>get(Record_.date), startDate);
	}

	public static Specification<Record> lessThanOrEqualToEndDate(final Date endDate) {
		return (Root<Record> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> cb
				.lessThanOrEqualTo(root.<Date>get(Record_.date), endDate);
	}

	public static Specification<Record> greaterThanOrEqualToMoney(final int startMoney) {
		return (Root<Record> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> cb
				.greaterThanOrEqualTo(root.<Integer>get(Record_.money), startMoney);
	}

	public static Specification<Record> lessThanOrEqualToMoney(final int endMoney) {
		return (Root<Record> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> cb
				.lessThanOrEqualTo(root.<Integer>get(Record_.money), endMoney);
	}
}
