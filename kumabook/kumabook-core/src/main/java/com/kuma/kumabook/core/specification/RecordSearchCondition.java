package com.kuma.kumabook.core.specification;

import static com.kuma.kumabook.core.specification.RecordSpecifications.alwaysTrue;
import static com.kuma.kumabook.core.specification.RecordSpecifications.equalRecordTypeName;
import static com.kuma.kumabook.core.specification.RecordSpecifications.equalToolTypeName;
import static com.kuma.kumabook.core.specification.RecordSpecifications.greaterThanOrEqualToMoney;
import static com.kuma.kumabook.core.specification.RecordSpecifications.greaterThanOrEqualToStartDate;
import static com.kuma.kumabook.core.specification.RecordSpecifications.lessThanOrEqualToEndDate;
import static com.kuma.kumabook.core.specification.RecordSpecifications.lessThanOrEqualToMoney;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.apache.commons.lang3.StringUtils;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;

import com.kuma.kumabook.core.model.Record;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RecordSearchCondition extends BaseSearchCondition<Record> implements Serializable {
	private static final long serialVersionUID = 1L;

	private Long userId;

	private Date fromDate;

	private Date toDate;

	private String toolType;

	private String recordType;

	private String name;

	private Integer fromMoney;

	private Integer toMoney;

	@Override
	public Specification<Record> getSpecification() {

		return (Root<Record> root, CriteriaQuery<?> query, CriteriaBuilder cb) -> {
			Specifications<Record> conditions = Specifications.where(alwaysTrue());

			conditions = conditions.and(RecordSpecifications.equalUserId(userId));

			if (StringUtils.isNotEmpty(name)) {
				conditions = conditions.and(RecordSpecifications.likeName(name));
			}

			if (StringUtils.isNotEmpty(toolType)) {
				conditions = conditions.and(equalToolTypeName(toolType));
			}

			if (StringUtils.isNotEmpty(recordType)) {
				conditions = conditions.and(equalRecordTypeName(recordType));
			}

			if (fromDate != null) {
				conditions = conditions.and(greaterThanOrEqualToStartDate(fromDate));
			}

			if (toDate != null) {
				conditions = conditions.and(lessThanOrEqualToEndDate(toDate));
			}

			if (fromMoney != null) {
				conditions = conditions.and(greaterThanOrEqualToMoney(fromMoney));
			}

			if (toMoney != null) {
				conditions = conditions.and(lessThanOrEqualToMoney(toMoney));
			}

			return conditions.toPredicate(root, query, cb);
		};
	}
}
