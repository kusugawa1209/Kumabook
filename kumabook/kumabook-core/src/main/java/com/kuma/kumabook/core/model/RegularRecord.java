package com.kuma.kumabook.core.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "REGULAR_RECORD")
public class RegularRecord implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;

	@ManyToOne
	@JoinColumn(name = "USER_ID", nullable = false)
	private User user;

	@Column(name = "NAME", length = 255, nullable = false)
	private String name;

	@Column(name = "MONEY", nullable = false)
	private int money;
	
	@Column(name = "NOTE", length = 255, nullable = true)
	private String note;

	@Column(name = "RECORD_TYPE", length = 50, nullable = false)
	private String recordType;
	
	@Column(name = "TOOL_TYPE", length = 50, nullable = false)
	private String toolType;

	@Column(name = "CREATION_DATE", nullable = false)
	private Date creationDate;

	@Column(name = "MODIFICATION_DATE", nullable = false)
	private Date modificationDate;

}
