package com.kuma.kumabook.core.vo;

import java.io.Serializable;

import lombok.Data;

@Data
public class ToolType implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;

	private String displayName;
	
	public ToolType(String name, String displayName) {
		this.name = name;
		this.displayName = displayName;
	}
	
}
