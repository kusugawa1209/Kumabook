package com.kuma.kumabook.core.model;

import java.io.Serializable;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import lombok.NoArgsConstructor;

@NoArgsConstructor
@StaticMetamodel(User.class)
public class User_ implements Serializable {

	private static final long serialVersionUID = 1L;

	public static volatile SingularAttribute<User, Long> id;

	public static volatile SingularAttribute<User, String> username;
}
