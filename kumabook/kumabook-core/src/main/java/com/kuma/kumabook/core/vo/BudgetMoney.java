package com.kuma.kumabook.core.vo;

import static com.kuma.kumabook.base.enums.BudgetMoneyType.CASH;
import static com.kuma.kumabook.base.enums.BudgetMoneyType.CREDIT;

import java.io.Serializable;

import com.kuma.kumabook.base.enums.BudgetMoneyType;

import lombok.Data;

@Data
public class BudgetMoney implements Serializable {

	private static final long serialVersionUID = 1L;

	private int sum = 0;

	private int credit = 0;

	private int cash = 0;

	public void addMoney(BudgetMoneyType budgetMoneyType, int money) {
		if (CASH.equals(budgetMoneyType)) {
			this.cash += money;
		}

		if (CREDIT.equals(budgetMoneyType)) {
			this.credit += money;
		}

		this.sum += money;
	}

	public BudgetMoney add(BudgetMoney budgetMoney) {
		BudgetMoney addedBudgetMoney = new BudgetMoney();
		if (budgetMoney != null) {
			addedBudgetMoney = budgetMoney;
		}

		BudgetMoney result = new BudgetMoney();

		result.setSum(this.sum + addedBudgetMoney.getSum());
		result.setCredit(this.credit + addedBudgetMoney.getCredit());
		result.setCash(this.cash + addedBudgetMoney.getCash());

		return result;
	}

	public BudgetMoney minus(BudgetMoney budgetMoney) {
		BudgetMoney addedBudgetMoney = new BudgetMoney();
		if (budgetMoney != null) {
			addedBudgetMoney = budgetMoney;
		}

		BudgetMoney result = new BudgetMoney();

		result.setSum(this.sum - addedBudgetMoney.getSum());
		result.setCredit(this.credit - addedBudgetMoney.getCredit());
		result.setCash(this.cash - addedBudgetMoney.getCash());

		return result;
	}

	public void minusMoney(BudgetMoneyType budgetMoneyType, int money) {
		if (CASH.equals(budgetMoneyType)) {
			this.cash -= money;
		}

		if (CREDIT.equals(budgetMoneyType)) {
			this.credit -= money;
		}

		this.sum -= money;
	}
}
