package com.kuma.kumabook.core.service;

import java.util.List;

import com.kuma.kumabook.core.vo.RecordType;

public interface RecordTypeService {

	public List<RecordType> findAll();

	public List<RecordType> findRegularableRecordTypes();
}