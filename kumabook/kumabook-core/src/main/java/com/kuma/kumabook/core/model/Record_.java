package com.kuma.kumabook.core.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

import lombok.NoArgsConstructor;

@NoArgsConstructor
@StaticMetamodel(Record.class)
public class Record_ implements Serializable {

	private static final long serialVersionUID = 1L;

	public static volatile SingularAttribute<Record, Long> id;

	public static volatile SingularAttribute<Record, User> user;

	public static volatile SingularAttribute<Record, Date> date;

	public static volatile SingularAttribute<Record, String> itemName;

	public static volatile SingularAttribute<Record, Integer> money;

	public static volatile SingularAttribute<Record, String> recordType;

	public static volatile SingularAttribute<Record, String> toolType;

	public static volatile SingularAttribute<Record, Integer> layaway;

	public static volatile SingularAttribute<Record, String> note;

	public static volatile SingularAttribute<Record, Date> creationDate;

	public static volatile SingularAttribute<Record, Date> modificationDate;

	public static volatile SingularAttribute<Record, Boolean> isAutoAdded;

}
