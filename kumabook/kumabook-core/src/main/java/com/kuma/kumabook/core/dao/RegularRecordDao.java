package com.kuma.kumabook.core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kuma.kumabook.core.model.RegularRecord;

@Repository
public interface RegularRecordDao extends JpaRepository<RegularRecord, Long> {
	
	@Query("SELECT r FROM RegularRecord r WHERE r.user.id = ?1 ORDER BY r.modificationDate DESC")
	public List<RegularRecord> findByUserId(long userId);
	
	@Query("SELECT r FROM RegularRecord r "
			+ "WHERE r.user.id = ?1 "
			+ "AND r.toolType = ?2")
	public List<RegularRecord> findByUserIdAndToolTypeName(long userId, String toolTypeName);
	
	@Query("SELECT COUNT(r.id) FROM RegularRecord r WHERE r.user.id = ?1")
	public Integer getCountByUserId(long userId);
}
