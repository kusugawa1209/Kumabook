package com.kuma.kumabook.mail.resolver;

import java.util.List;

import javax.mail.Message;

import com.kuma.kumabook.core.model.CreditCard;
import com.kuma.kumabook.core.vo.MailRecord;

public interface GmailMailResolver {
	
	public List<MailRecord> retriveMailRecordsFromUnreadMessage(List<CreditCard> creditCards, Message message);
	
}
