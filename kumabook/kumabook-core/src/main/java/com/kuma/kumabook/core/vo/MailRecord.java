package com.kuma.kumabook.core.vo;

import java.io.Serializable;
import java.util.Date;

import lombok.Data;

@Data
public class MailRecord implements Serializable {

	private static final long serialVersionUID = 1L;

	private Date date;

	private String toolTypeName;
	
	private int money;

	private String item;

	private String note;
}
