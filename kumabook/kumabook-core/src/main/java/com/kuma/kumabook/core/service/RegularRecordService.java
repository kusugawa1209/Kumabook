package com.kuma.kumabook.core.service;

import java.util.Collection;
import java.util.List;

import com.kuma.kumabook.base.queryondition.PageCondition;
import com.kuma.kumabook.core.model.RegularRecord;
import com.kuma.kumabook.core.model.User;

public interface RegularRecordService {

	public RegularRecord getRegularRecordById(long regularRecordId);

	public int getRegularRecordsCountByUserId(long userId);

	public List<RegularRecord> findRegularRecordsByUserId(long userId);

	public List<RegularRecord> findRegularRecordsByUserId(long userId, PageCondition pageCondition);

	public List<RegularRecord> findRegularRecordsByUserIdAndToolTypeName(long userId, String toolTypeName);

	public RegularRecord saveRegularRecord(RegularRecord regularRecord);

	public RegularRecord saveRegularRecord(User user, RegularRecord regularRecord);

	public List<RegularRecord> saveRegularRecords(Collection<RegularRecord> regularRecords);

	public void removeRegularRecord(RegularRecord regularRecord);

}