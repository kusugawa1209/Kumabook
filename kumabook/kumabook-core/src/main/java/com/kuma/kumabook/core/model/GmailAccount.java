package com.kuma.kumabook.core.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
@Entity
@Table(name = "GMAIL_ACCOUNT")
public class GmailAccount implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long id;
	
	@Column(name = "USER_ACCOUNT")
	private String userAccount;
	
	@Column(name = "PASSWORD")
	private String password;
	
	@OneToOne
	@JoinColumn(name = "USER_ID", nullable = false)
	private User user;
}
