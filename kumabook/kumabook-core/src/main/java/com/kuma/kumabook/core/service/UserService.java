package com.kuma.kumabook.core.service;

import java.util.List;

import com.kuma.kumabook.core.model.User;

public interface UserService {
	
	public List<User> findUsers();

	public User getUserById(long id);

	public User createAndSaveUser(long id, String username);
	
	public User saveUser(User user);
}
