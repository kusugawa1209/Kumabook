package com.kuma.kumabook.core.service;

import com.kuma.kumabook.core.vo.Assets;

public interface AssetsService {
	public Assets getAssetsByUserId(long userId);
}