package com.kuma.kumabook.core.specification;

import java.io.Serializable;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

public interface SearchCondition<T> extends Serializable {
	Pageable getPageable();

	Specification<T> getSpecification();
}
