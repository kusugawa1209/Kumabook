package com.kuma.kumabook.core.vo;

import java.io.Serializable;

import com.kuma.kumabook.base.enums.RecordTypeEnum;

import lombok.Data;

@Data
public class RecordType implements Serializable {

	private static final long serialVersionUID = 1L;

	private String name;

	private String displayName;

	public RecordType(RecordTypeEnum recordTypeEnum) {
		this.name = recordTypeEnum.name();
		this.displayName = recordTypeEnum.getDisplayName();
	}
}
