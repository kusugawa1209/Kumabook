package com.kuma.kumabook.core.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name = "CREDIT_CARD")
public class CreditCard implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "ID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	@JoinColumn(name = "USER_ID", nullable = false)
	private User user;

	@Column(name = "BILLING_DAY", nullable = false)
	private int billingDay;

	@Column(name = "NAME", length = 255, nullable = false)
	private String name;

	@Column(name = "CREATION_DATE", nullable = false)
	private Date creationDate;

	@Column(name = "MODIFICATION_DATE", nullable = false)
	private Date modificationDate;

}
