package com.kuma.kumabook.core.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kuma.kumabook.core.model.CreditCard;

@Repository
public interface CreditCardDao extends JpaRepository<CreditCard, Long> {

	@Query("SELECT c FROM CreditCard c WHERE c.user.id = ?1 ORDER BY c.creationDate DESC")
	public List<CreditCard> findByUserId(long userId);

	@Query("SELECT COUNT(c.id) FROM CreditCard c WHERE c.user.id = ?1")
	public Integer getCountByUserId(long userId);
}
