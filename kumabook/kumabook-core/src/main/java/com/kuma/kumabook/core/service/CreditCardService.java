package com.kuma.kumabook.core.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.kuma.kumabook.base.queryondition.PageCondition;
import com.kuma.kumabook.core.model.CreditCard;
import com.kuma.kumabook.core.model.User;

public interface CreditCardService {
	
	public Map<Long, Date> getCurrentBillingDatesByUserId(long userId, Date currentDate);

	public Map<Long, Integer> getBillingDaysByUserId(long userId);
	
	public CreditCard getCreditCardById(long cardId);
	
	public List<CreditCard> findCreditCardsByUserId(long userId);

	public List<CreditCard> findCreditCardsByUserId(long userId, PageCondition pageCondition);

	public int getCreditCardsCountByUserId(long userId);
	
	public CreditCard saveCreditCard(User user, CreditCard creditCard);

	public CreditCard saveCreditCard(CreditCard creditCard);
	
	public void removeCreditCard(CreditCard creditCard);

}