package com.kuma.kumabook.core.service;

import java.util.List;

import com.kuma.kumabook.core.vo.Budget;

public interface BudgetService {
	public Budget getTodayBudgetByUserId(long userId);
	
	public Budget getMonthlyBudgetByUserId(long userId);

	public List<Budget> findAllBudgets(long userId, int predictMonthCount);
}