package com.kuma.kumabook.core.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.kuma.kumabook.core.model.Record;

@Repository
public interface RecordDao extends JpaRepository<Record, Long>, JpaSpecificationExecutor<Record> {

	@Query("SELECT r FROM Record r WHERE r.user.id = ?1 ORDER BY r.date DESC")
	public List<Record> findByUserId(long userId);

	@Query("SELECT r FROM Record r " + "WHERE r.user.id = ?1 " + "AND r.toolType = ?2")
	public List<Record> findByUserIdAndToolTypeName(long userId, String toolTypeName);

	@Query("SELECT COUNT(r.id) FROM Record r WHERE r.user.id = ?1")
	public Integer getCountByUserId(long userId);

	@Query("SELECT r FROM Record r WHERE r.user.id = ?1 AND r.isAutoAdded = true")
	public List<Record> findAutoAddedRecords(long userId);

	@Query("SELECT r FROM Record r " + "WHERE r.user.id = ?1 " + "AND r.date BETWEEN ?2 AND ?3 "
			+ "AND (r.toolType = 'CASH' OR r.toolType = 'TRANSFER')")
	public List<Record> findNonCreditRecordsByUserIdAndDates(long userId, Date afterDate, Date beforeDate);
	
	@Query("SELECT r FROM Record r WHERE r.user.id = ?1 AND (r.toolType = 'CASH' OR r.toolType = 'TRANSFER') ORDER BY r.date ASC")
	public List<Record> findNonCreditRecordsByUserId(long userId);

	@Query("SELECT r FROM Record r WHERE r.user.id = ?1 AND (r.toolType != 'CASH' AND r.toolType != 'TRANSFER') ORDER BY r.date ASC")
	public List<Record> findCreditRecordsByUserId(long userId);
}
