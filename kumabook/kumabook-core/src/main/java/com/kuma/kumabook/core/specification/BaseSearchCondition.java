package com.kuma.kumabook.core.specification;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

public abstract class BaseSearchCondition<T> implements SearchCondition<T> {
	private static final long serialVersionUID = 1L;

	private Integer page;

	private Integer size;

	private Sort sort;

	public Pageable getPageable() {

		if (page == null && size == null) {
			return null;
		}

		if (sort == null) {
			return new PageRequest(page, size);
		}

		return new PageRequest(page, size, sort);
	}
}
