package com.kuma.kumabook.core.vo;

import static com.kuma.kumabook.base.enums.RecordTypeEnum.INCOME;
import static com.kuma.kumabook.base.enums.RecordTypeEnum.OUTGOING;
import static com.kuma.kumabook.base.enums.RecordTypeEnum.SAVING;
import static com.kuma.kumabook.base.enums.RecordTypeEnum.WITHDRAW;

import java.io.Serializable;

import com.kuma.kumabook.base.enums.BudgetMoneyType;
import com.kuma.kumabook.base.enums.RecordTypeEnum;

import lombok.Data;

@Data
public class Budget implements Serializable {

	private static final long serialVersionUID = 1L;

	private int year;

	private int month;

	private BudgetMoney income = new BudgetMoney();

	private BudgetMoney outgoing = new BudgetMoney();

	private BudgetMoney saving = new BudgetMoney();

	private BudgetMoney withdraw = new BudgetMoney();

	private BudgetMoney remain = new BudgetMoney();

	private int percent = 0;

	public void addMoney(BudgetMoneyType moneyType, RecordTypeEnum recordType, int money) {
		if (INCOME.equals(recordType)) {
			this.getIncome().addMoney(moneyType, money);
		} else if (OUTGOING.equals(recordType)) {
			this.getOutgoing().addMoney(moneyType, money);
		} else if (SAVING.equals(recordType)) {
			this.getSaving().addMoney(moneyType, money);
		} else if (WITHDRAW.equals(recordType)) {
			this.getWithdraw().addMoney(moneyType, money);
		}
	}

	public Budget copy() {
		Budget newBudget = new Budget();

		newBudget.setYear(this.getYear());
		newBudget.setMonth(this.getMonth());
		newBudget.setIncome(this.getIncome());
		newBudget.setOutgoing(this.getOutgoing());
		newBudget.setSaving(this.getSaving());
		newBudget.setWithdraw(this.getWithdraw());
		newBudget.setRemain(this.getRemain());
		newBudget.setPercent(this.getPercent());

		return newBudget;
	}
}
