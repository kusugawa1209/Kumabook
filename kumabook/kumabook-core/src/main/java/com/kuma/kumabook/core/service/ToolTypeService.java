package com.kuma.kumabook.core.service;

import java.util.List;

import com.kuma.kumabook.core.vo.ToolType;

public interface ToolTypeService {

	public List<ToolType> findToolTypesByUserId(long userId);
}